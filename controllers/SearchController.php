<?php

namespace app\controllers;
use yii\web\Controller;
use yii\filters\AccessControl;

class SearchController extends Controller
{
    public function actionSearchResult()
    {
    	$searchData = $this->getSearchParams();
    	$propertySearch = new \app\models\PropertySearch();
		$searchResult = $propertySearch->getPropertyList($searchData);
		if($searchResult)
		{
             if (\Yii::$app->request->isAjax) 
             {
                if($searchData['displayview'] == 'grid')
                {
                    return $this->renderAjax('searchdatagrid',['result' => $searchResult]);
                }
                else
                {
                    return $this->renderAjax('searchdatalist',['result' => $searchResult]);
                }
             }
             else
             {
    			return $this->render('searchresult',['result' => $searchResult]);
             }
		}
		return $this->redirect(\Yii::$app->request->referrer); 
    }

    public function getSearchParams()
    {
        $params = \Yii::$app->request->getQueryParams();
        if(empty($params))
        {
            $params = \Yii::$app->request->getBodyParams();
        }
        $searchData['city_name'] = $params['city_name'];
        $searchData['lat'] = $params['lat'];
        $searchData['long'] = $params['long'];
        $searchData['state_name'] = $params['state_name'];
        $searchData['state_slug'] = strtolower(str_replace(' ', '_', $searchData['state_name']));
        $searchData['location'] = $params['location'];
        if(isset($params['rooms_type']))
        {
            $searchData['rooms_type'] = $params['rooms_type'];
        }
        if(isset($params['rooms_for']))
        {
            $searchData['rooms_for'] = $params['rooms_for'];
        }
        if(!empty($params['locality']))
        {
            $searchData['locality'] = $params['locality'];
        }
        if(isset($params['tag_premium'])){
            $searchData['tag_premium'] = $params['tag_premium'];
        }
        if(isset($params['tag_flagship'])){
            $searchData['tag_flagship'] = $params['tag_flagship'];
        }
        if(isset($params['tag_economy'])){
        $searchData['tag_economy'] = $params['tag_economy'];
        }
        if(isset($params['fullyfur'])){
            $searchData['fullyfur'] = $params['fullyfur'];
        }
        if(isset($params['semifur'])){
            $searchData['semifur'] = $params['semifur'];
        }
        if(isset($params['unfur'])){
            $searchData['unfur'] = $params['unfur'];
        }
        if(!empty($params['lowlimit']))
        {
            $searchData['lowlimit'] = $params['lowlimit'];
        }
        if(!empty($params['uplimit']))
        {
            $searchData['uplimit'] = $params['uplimit'];
        }
        if(!empty($params['sort']))
        {
            $searchData['sort'] = $params['sort'];
        }
        if(!empty($params['displayview']))
        {
            $searchData['displayview'] = $params['displayview'];
        }
        $state = new \app\models\State();
        $searchData['current_state'] = $state::find()->where(['state_slug' => $searchData['state_slug'],'del_status' => '0'])->one();
        return $searchData;
    }
}
