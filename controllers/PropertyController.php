<?php

namespace app\controllers;
use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\MubAdmin\modules\RealEstate\models\Property;
use app\modules\MubAdmin\modules\RealEstate\models\Room;
use app\models\MubUser;
use app\models\HomesListing;

class PropertyController extends Controller
{
    public function actionPropertyDetails($name)
    {
    $propertyName = strtoupper(str_replace('-',' ',$name));
    $property =  new \app\modules\MubAdmin\modules\RealEstate\models\Property();
		$propertyData = $property::find()->where(['property_name' => $propertyName,'del_status' => '0'])->one();
		$similarProperty = $this->getSimilarProperty($propertyData);
		return $this->render('property-details',['property' => $propertyData,'similarProperty' => $similarProperty]);
    }

    public function actionCancelVisit()
    {
  		$scheduledVisits = new \app\models\ScheduledVisits();
  		$param = \Yii::$app->request->getBodyParam('scheduid');
  		$currentVisit = $scheduledVisits::findOne($param);
  		$currentVisit->status = 'cancelled';
  		$property = Property::findOne($currentVisit->property_id);
  		$propertyOwner = MubUser::findOne($property->mub_user_id);
  		$ownerContact = $propertyOwner->mubUserContacts;
  		if(!$currentVisit->save(false))
  		{
  			p($currentVisit->getErrors());
  		}
  		\Yii::$app->mailer->compose('cancelmail',['property' => $property,'currentVisit' => $currentVisit])
      ->setFrom('info@osmstays.com')
      ->setTo($currentVisit->email)
      ->setSubject('Scheduled Visit Cancelled')
      ->setTextBody('Plain text content')
      ->setCc('info@osmstays.com')
      ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
      ->send();
  		return true;
    }

    public function actionAllRooms($propId)
    {
    	$this->layout = false;
    	$property = Property::findOne($propId);
    	return $this->render('allrooms',['property' => $property]);
    }

    public function actionAllBeds($propId)
    {
      $this->layout = false;
      $property = Property::findOne($propId);
      return $this->render('allbeds',['property' => $property]);
    }

    public function actionPaynow()
    {
      $this->layout = false;
      $model = new \app\models\Booking();
      return $this->render('paynow',['model' => $model]);
    }

    public function actionFailure()
    {
      return $this->render('failure');
    }

    public function actionCalculateTax()
    {
      if(\Yii::$app->request->isAjax)
      {
        $price = \Yii::$app->request->getBodyParams()['price'];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(is_numeric($price))
        {
          $tax = \app\helpers\TaxHelper::calculateTax('3.5',$price);
          $response['tax'] = $tax;
          $response['amount'] = $price+$tax;
          return $response;
        }   
      }
     return false;
    }

    public function actionNewListing()
    {
    	if (\Yii::$app->request->isAjax)
    	{
	    	// $this->layout = false;
	      	$homeListing = new HomesListing(); 
	      	return $this->renderAjax('newlisting',['homeListing' => $homeListing]);	
    	}
    	
    }

    public function actionSuccess()
    {
      return $this->render('success');
    }

    public function getSimilarProperty($property)
    {
    	$propertySearch  = new \app\models\PropertySearch();
    	return $propertySearch->getSimilarProperty($property);
    }
}
