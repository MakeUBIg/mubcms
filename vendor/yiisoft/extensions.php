<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.17.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  '2amigos/yii2-tinymce-widget' => 
  array (
    'name' => '2amigos/yii2-tinymce-widget',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@dosamigos/tinymce' => $vendorDir . '/2amigos/yii2-tinymce-widget/src',
    ),
  ),
  'yiister/yii2-gentelella' => 
  array (
    'name' => 'yiister/yii2-gentelella',
    'version' => '1.2.1.0',
    'alias' => 
    array (
      '@yiister/gentelella' => $vendorDir . '/yiister/yii2-gentelella',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.9.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  '2amigos/yii2-gallery-widget' => 
  array (
    'name' => '2amigos/yii2-gallery-widget',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@dosamigos/gallery' => $vendorDir . '/2amigos/yii2-gallery-widget/src',
    ),
  ),
  '2amigos/yii2-file-upload-widget' => 
  array (
    'name' => '2amigos/yii2-file-upload-widget',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@dosamigos/fileupload' => $vendorDir . '/2amigos/yii2-file-upload-widget/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.1.2.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'demi/sitemap-generator' => 
  array (
    'name' => 'demi/sitemap-generator',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@demi/sitemap' => $vendorDir . '/demi/sitemap-generator',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
    ),
  ),
  'asofter/yii2-imperavi-redactor' => 
  array (
    'name' => 'asofter/yii2-imperavi-redactor',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/imperavi' => $vendorDir . '/asofter/yii2-imperavi-redactor/yii/imperavi',
    ),
  ),
  'madand/yii2-underscore' => 
  array (
    'name' => 'madand/yii2-underscore',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@madand/underscore' => $vendorDir . '/madand/yii2-underscore/src',
    ),
  ),
  'cedx/yii2-mustache' => 
  array (
    'name' => 'cedx/yii2-mustache',
    'version' => '6.0.0.0',
    'alias' => 
    array (
      '@yii/mustache' => $vendorDir . '/cedx/yii2-mustache/lib',
    ),
  ),
  'xfg/yii2-mustache.js' => 
  array (
    'name' => 'xfg/yii2-mustache.js',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@xfg/mustachejs' => $vendorDir . '/xfg/yii2-mustache.js',
    ),
  ),
  'perminder-klair/yii2-sir-trevor-js' => 
  array (
    'name' => 'perminder-klair/yii2-sir-trevor-js',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kato/sirtrevorjs' => $vendorDir . '/perminder-klair/yii2-sir-trevor-js',
    ),
  ),
);
