<?php

namespace app\modules\MubAdmin\modules\RealEstate\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\MubAdmin\modules\RealEstate\models\Room;
use app\modules\MubAdmin\modules\RealEstate\models\RoomSearch;
use app\modules\MubAdmin\modules\RealEstate\models\RoomProcess;

/**
 * RoomController implements the CRUD actions for ProductCategory model.
 */
class RoomController extends MubController
{
   public function getPrimaryModel()
   {
        return new Room();
   }

   public function getProcessModel()
   {
        return new RoomProcess();
   }

   public function getSearchModel()
   {
        return new RoomSearch();
   }
}
