<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\RealEstate\models\PropertySupportStaff */

$this->title = 'Update Property Support Staff: ' . $supportStaff->staff_name;
$this->params['breadcrumbs'][] = ['label' => 'Property Support Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $supportStaff->staff_name, 'url' => ['view', 'id' => $supportStaff->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="property-support-staff-update">
	<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'supportStaff' => $supportStaff,
        'activeProperties' => $activeProperties,
        'allStates' => $allStates
    ]) ?>
</div>
