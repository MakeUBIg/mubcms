<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $bed app\modules\MubAdmin\modules\RealEstate\beds\Bed */

$this->title = 'Update Bed: ' . $bed->id;
$this->params['breadcrumbs'][] = ['label' => 'Beds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $bed->id, 'url' => ['view', 'id' => $bed->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bed-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'bed' => $bed,
    ]) ?>

</div>
