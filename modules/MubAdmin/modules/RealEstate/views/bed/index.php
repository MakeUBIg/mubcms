<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\MubAdmin\modules\RealEstate\models\Room;

/* @var $this yii\web\View */
/* @var $searchbed app\modules\MubAdmin\modules\RealEstate\beds\BedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Beds';
$this->params['breadcrumbs'][] = $this->title;
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
$roomId = \Yii::$app->request->getQueryParam('room');
$room = Room::findOne($roomId);
?>
<div class="bed-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['bed' => $searchbed]); ?>

    <p>
     <?= Html::a('<- Back to all Rooms', ['/mub-admin/real-estate/room/index','property' => $room->property_id,'state' => $stateId], ['class' => 'btn btn-danger']); ?>
        <?= Html::a('Create Bed', ['create','room' => $roomId,'state' => $stateId], ['class' => 'btn btn-success']); ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{items}\n<div align='center'>{pager}</div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'available',
            'filter' => Html::activeDropDownList($searchModel,'available',['0' => 'Not Available','1' => 'Available'],['prompt' =>'Select','class' => 'form-control']),
            'value' => function($model)
            {
                return ($model->available == '0') ? 'Not Available' : 'Available';    
            }
            ],
            'room_id',
            // 'last_booked',
            'price',
            // 'availability_date',
            'type',
            // 'del_status',

            ['header' => 'Actions','class' => 'app\components\MubActionColumn'],
        ],
    ]); ?>
</div>
