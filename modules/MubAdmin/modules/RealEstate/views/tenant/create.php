<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $tenant app\modules\MubAdmin\modules\RealEstate\tenants\Tenant */

$this->title = 'Create Tenant';
$this->params['breadcrumbs'][] = ['label' => 'Tenants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-create">
	<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'tenant' => $tenant,
        'activeProperties' => $activeProperties,
        'allStates' => $allStates
    ]) ?>

</div>
