<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $amenity app\modules\MubAdmin\modules\RealEstate\amenitys\Amenity */

$this->title = 'Amenity :'.  $amenity->amenity_name;
$this->params['breadcrumbs'][] = ['label' => 'Amenities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amenity-view">
<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="col-md-10 col-md-offset-1">
    <p>
        <?= Html::a('Delete', ['delete', 'id' => $amenity->id], ['class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('All Amenities', ['index'], ['class' => 'btn btn-primary']) ?>
    </p>
    </div>
<div class="col-md-6 col-md-offset-1">
    <?= DetailView::widget([
        'model' => $amenity,
        'attributes' => [
            'icon_url',
            'amenity_category',
            'amenity_name',
            'status'
            
        ],
    ]) ?>
</div>
</div>
