<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $room app\modules\MubAdmin\modules\RealEstate\rooms\RoomSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($room, 'id') ?>

    <?= $form->field($room, 'mub_user_id') ?>

    <?= $form->field($room, 'property_id') ?>

    <?= $form->field($room, 'beds_count') ?>

    <?= $form->field($room, 'room_number') ?>

    <?php // echo $form->field($room, 'room_name') ?>

    <?php // echo $form->field($room, 'room_slug') ?>

    <?php // echo $form->field($room, 'room_type') ?>

    <?php // echo $form->field($room, 'price') ?>

    <?php // echo $form->field($room, 'extrafield1') ?>

    <?php // echo $form->field($room, 'extrafield2') ?>

    <?php // echo $form->field($room, 'created_at') ?>

    <?php // echo $form->field($room, 'updated_at') ?>

    <?php // echo $form->field($room, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
