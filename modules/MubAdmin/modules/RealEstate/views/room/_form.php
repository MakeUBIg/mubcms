<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $room app\modules\MubAdmin\modules\RealEstate\rooms\Room */
/* @var $form yii\widgets\ActiveForm */
$propertyId = \Yii::$app->request->getQueryParam('property');
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
?>

<div class="room-form">

    <?php $form = ActiveForm::begin(); ?>
     <div class="row">
     <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($room, 'room_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($room, 'room_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($room, 'room_type')->dropDownList(['furnished' => 'Fully Furnished','semifurnished' => 'Semi Furnished','unfurnished' => 'Un-Furnished'], ['prompt' => 'Select Room Type']) ?>

    <?= $form->field($room,'property_id')->hiddenInput(['value' => $propertyId])->label(false);?>

    <?= $form->field($room, 'room_size')->dropDownList([ '1bhk' => '1 BHK', '2bhk' => '2 BHK', '3bhk' => '3 BHK', '4bhk' => '4 BHK', '1bk' => '1 BK', '2bk' => '2 BK', ], ['prompt' => 'Select Room Size']) ?>

    <?= $form->field($room, 'price')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($room->isNewRecord ? 'Create' : 'Update', ['class' => $room->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if(!$room->isNewRecord){?>
        <?= Html::a('Cancel', ['/mub-admin/real-estate/room','property' => $room->property_id,'state' => $stateId], ['class' => 'btn btn-warning cancel'])?>
         <?php }?>
    </div>
</div></div>
    <?php ActiveForm::end(); ?>

</div>
