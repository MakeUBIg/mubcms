<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $room app\modules\MubAdmin\modules\RealEstate\rooms\Room */

$this->title = $room->room_number;
$this->params['breadcrumbs'][] = ['label' => 'Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
?>
<div class="room-view">

<?= Html::a('Add/View Beds', ['/mub-admin/real-estate/bed', 'room' => $room->id,'state' => $stateId], ['class' => 'btn btn-info btn-lg pull-right'])?>
    <div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="col-md-6 col-md-offset-1">
    <?= DetailView::widget([
        'model' => $room,
        'attributes' => [
            'id',
            'property_id',
            'beds_count',
            'room_number',
            'room_name',
            'room_slug',
            'room_type',
            'price'
        ],
    ]) ?>
</div>
     <center>
     <div class="col-md-6 col-md-offset-1">
        <?= Html::a('Update', ['update', 'id' => $room->id,'state'=>$stateId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $room->id,'state' => $stateId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('All Rooms',['index','state' => $stateId,'property' => $room->property_id], [
            'class' => 'btn btn-warning cancel',
        ]) ?>
        </div>
</center>
</div>
