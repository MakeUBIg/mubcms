<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $property app\modules\MubAdmin\modules\RealEstate\propertys\Property */

$this->title = 'Update Property: ' . $property->property_name;
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $property->property_name, 'url' => ['view', 'id' => $property->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="property-update">
<div class="col-md-10 col-md-offset-1">

    <h1><?= Html::encode($this->title) ?></h1>
</div>
    <?= $this->render('_form', [
        'property' => $property,
        'propertyAmenity' => $propertyAmenity,
        'allAmenities' => $allAmenities
    ]) ?>

</div>
