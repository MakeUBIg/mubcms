<?php 
namespace app\modules\MubAdmin\modules\RealEstate\controllers;

use yii\bootstrap\ActiveForm;
$mubUserId = \app\models\User::getMubUserId();
$mubUserModel = new \app\models\MubUser();
$currentUser = $mubUserModel::findOne($mubUserId);
$userDetails = $currentUser;
$contactDetails = $currentUser->mubUserContacts;
?>
<div class="container">
     <div class="col-md-12 text-center" style="margin:1em;">
      <h3> Profile</h3>
      <h5 style="color: #6B7C8E;"><b style="color: red;">*</b> You can  change Password  from this page</h5>
      </div
      <div class="agent-grids">
      <div class="col-md-12 agent-grid">
         <div class="col-md-5"> 
           <img src="/images/family.png" style="width: 100%; margin-top: 1em;">
         </div>
         <div class="col-md-6 col-md-offset-1 agent-right" style="width: 50%; margin-top: 2em;">
            <?php $form = ActiveForm::begin(['layout' => 'horizontal','options' => ['method' => 'POST'],'action' => ['/mub-admin/real-estate/property/profile']]); ?>
            <?= $form->field($currentUser, 'first_name')->textInput(['autofocus' => true,'class' => 'form-control']);?>
           <?= $form->field($currentUser, 'last_name')->textInput(['class' => 'form-control']);?>
            <?= $form->field($currentUser, 'username')->textInput(['class' => 'form-control','readonly' => true]);?>
            <?= $form->field($currentUser, 'password')->passwordInput(['class' => 'form-control']);?>
            <?= $form->field($contactDetails, 'mobile')->textInput(['class' => 'form-control','maxlength' => 10]);?>
             <?= $form->field($contactDetails, 'email')->textInput(['class' => 'form-control','readonly' => true]);?>
             <?= $form->field($currentUser,'id')->hiddenInput()->label(false);?>
           <center><input type="submit" value="Update" style="margin-bottom: 2em!important;"></center>
           <?php ActiveForm::end(); ?>
         </div>
         <div class="clearfix"></div>
      </div>
</div>