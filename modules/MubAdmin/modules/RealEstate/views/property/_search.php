<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $property app\modules\MubAdmin\modules\RealEstate\propertys\PropertySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($property, 'id') ?>

    <?= $form->field($property, 'mub_user_id') ?>

    <?= $form->field($property, 'rooms_count') ?>

    <?= $form->field($property, 'lat') ?>

    <?= $form->field($property, 'long') ?>

    <?php // echo $form->field($property, 'city_name') ?>

    <?php // echo $form->field($property, 'state_id') ?>

    <?php // echo $form->field($property, 'property_name') ?>

    <?php // echo $form->field($property, 'property_slug') ?>

    <?php // echo $form->field($property, 'property_type') ?>

    <?php // echo $form->field($property, 'property_area') ?>

    <?php // echo $form->field($property, 'sa_a') ?>

    <?php // echo $form->field($property, 'sa_b') ?>

    <?php // echo $form->field($property, 'description') ?>

    <?php // echo $form->field($property, 'pincode') ?>

    <?php // echo $form->field($property, 'created_at') ?>

    <?php // echo $form->field($property, 'updated_at') ?>

    <?php // echo $form->field($property, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
