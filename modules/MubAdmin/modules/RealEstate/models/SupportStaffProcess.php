<?php 

namespace app\modules\MubAdmin\modules\RealEstate\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class SupportStaffProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $supportStaff = new PropertySupportStaff();
        $this->models = [
            'supportStaff' => $supportStaff
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $property = new Property();
        $state = new \app\models\State();
        $mubUserId = \app\models\User::getMubUserId();
        $where = ['mub_user_id' => $mubUserId,'status' => 'active','del_status' => '0'];
        $activeProperties = $property->getAll('property_name',$where);
        $allStates = $state->getAll('state_name');
        $where = ['del_status' => '0'];
        return 
        [
        'allStates' => $allStates,
        'activeProperties' => $activeProperties
        ];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [
            'supportStaff' => $model
        ];
        return $this->relatedModels;
    }

    public function saveSupportStaff($supportStaff)
    {
        $userId = \app\models\User::getMubUserId();
        $supportStaff->mub_user_id =  $userId;
        $supportStaff->support_staff_slug = \app\helpers\StringHelper::generateSlug($supportStaff->staff_name);
        return ($supportStaff->save()) ?$supportStaff->id :p($supportStaff->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['supportStaff']))
            {
            try {
                    $supportStaffId = $this->saveSupportStaff($data['supportStaff']);
                    if($supportStaffId)
                    {
                        return $supportStaffId;
                    }
                    p('supportStaff not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}