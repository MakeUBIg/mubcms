<?php 

namespace app\modules\MubAdmin\modules\RealEstate\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class AmenityProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $amenity = new Amenity();
        $this->models = [
            'amenity' => $amenity
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [
            'amenity' => $model
        ];
        return $this->relatedModels;
    }

    public function saveAmenity($amenity)
    {
        $userId = \app\models\User::getMubUserId();
        $amenity->mub_user_id =  $userId;
        $amenity->amenity_slug = StringHelper::generateSlug($amenity->amenity_name);
        return ($amenity->save()) ?$amenity->id :p($amenity->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['amenity']))
            {
            try {
                    $amenityId = $this->saveAmenity($data['amenity']);
                    if($amenityId)
                    {
                        return $amenityId;
                    }
                    p('amenity not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}