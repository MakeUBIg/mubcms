<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\RealEstate\models\Amenity;

/**
 * AmenitySearch represents the model behind the search form about `app\modules\MubAdmin\modules\RealEstate\models\Amenity`.
 */
class AmenitySearch extends Amenity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'amenity_price'], 'integer'],
            [['amenity_category', 'amenity_name', 'amenity_slug', 'status', 'icon_url', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Amenity::find()->where(['del_status' => '0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 10 ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'amenity_price' => $this->amenity_price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'amenity_category', $this->amenity_category])
            ->andFilterWhere(['like', 'amenity_name', $this->amenity_name])
            ->andFilterWhere(['like', 'amenity_slug', $this->amenity_slug])
            ->andFilterWhere(['like', 'icon_url', $this->icon_url])
            ->andFilterWhere(['=', 'amenity.status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
