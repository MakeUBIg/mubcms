<?php

namespace app\migrations;
use app\commands\Migration;

class m170430_055709_create_room extends Migration
{
   
    public function getTableName()
    {
        return 'room';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'property_id' => ['property','id']
        ];
    }

    
    public function getKeyFields()
    {
        return [
            'room_number' => 'room_number',
            'price'  =>  'price',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'property_id' => $this->integer()->notNull(),
            'beds_count' => $this->integer(),
            'room_number' => $this->string()->notNull(),
            'room_name' => $this->string(),
            'room_slug' => $this->string(),
            'room_size' => "enum('1bhk','2bhk','3bhk','4bhk','1bk','2bk') NOT NULL DEFAULT '2bhk'",
            'room_type' => "enum('furnished','semifurnished','unfurnished') NOT NULL DEFAULT 'semifurnished'",
            'price' => $this->integer()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'extrafield1' => $this->string(),
            'extrafield2' => $this->string(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
