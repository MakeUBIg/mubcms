<?php

namespace app\migrations;
use app\commands\Migration;

class m170603_113349_create_scheduled_visits extends Migration
{
    public function getTableName()
    {
        return 'scheduled_visits';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'property_id' => ['property', 'id']
        ];
    }

    public function getKeyFields()
    {
        return [
                'email' => 'email',
                'mobile' => 'mobile',
                'occupancy' => 'occupancy'
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'property_id' => $this->integer()->notnull(),
            'name' => $this->string(50)->notNull(),
            'email' => $this->string(50)->notNull(),
            'mobile' => $this->string(50)->notNull(), 
            'occupancy' => "enum('singleroom','doubleshare','tripleshare') NOT NULL DEFAULT 'doubleshare'", 
            'status' => "enum('scheduled','done','cancelled') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT 'scheduled'",
            'scheduled_time' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
