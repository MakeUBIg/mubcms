<?php

namespace app\migrations;
use app\commands\Migration;

class m170607_060228_create_booking extends Migration
{
    public function getTableName()
    {
        return 'booking';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'property_id' => ['property', 'id']
        ];
    }

    public function getKeyFields()
    {
        return [
                'email' => 'email',
                'mobile' => 'mobile',
                'occupancy' => 'occupancy'
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'property_id' => $this->integer()->notnull(),
            'name' => $this->string(50)->notNull(),
            'email' => $this->string(50)->notNull(),
            'mobile' => $this->string(50)->notNull(),
            'booking_id' => $this->string()->notNull(), 
            'occupancy' => "enum('singleroom','doubleshare','tripleshare') NOT NULL DEFAULT 'doubleshare'", 
            'amount' => $this->integer(),
            'actual_price' => $this->integer(),
            'tax_amount' => $this->integer(),
            'payment_mode' => $this->string(),
            'status' => "enum('booking','done','cancelled') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT 'booking'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
