<?php

namespace app\migrations;
use app\commands\Migration;

class m170617_155200_create_new_homes_listing extends Migration
{
    public function getTableName()
    {
        return 'homes_listing';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id']
        ];
    }

    public function getKeyFields()
    {
        return[];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer(),
            'property_type' => "enum('hostel','pg','flat','house') NOT NULL DEFAULT 'pg'",
            'property_area' => "enum('boys','girls','both','family') NOT NULL DEFAULT 'boys'",
            'owner_name' => $this->string()->notNull(),
            'email' => $this->string(),
            'mobile' => $this->string(),
            'sa_a' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }

}
