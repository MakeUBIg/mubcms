<?php

namespace app\migrations;
use app\commands\Migration;

class m170518_072502_create_tenant extends Migration
{
    public function getTableName()
    {
        return 'tenant';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'property_id' => ['property','id'],
        ];
    }

    public function getKeyFields()
    {
        return [
                'email' => 'email',
                'mobile' => 'mobile',
                'joining_date' => 'joining_date',
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'property_id' => $this->integer(),

            'name' => $this->string(50)->notNull(),
            'f_name' => $this->string(50),
            'email' => $this->string(50),

            'mobile' => $this->integer()->notNull(),
            'f_mobile' => $this->integer(),
            'state_id' => $this->integer(),
            'city_name' => $this->integer(),
            'monthly_rent' => $this->integer(),
            'security' => $this->integer(),

            'sa_home' => $this->text(),
            'sa_office' => $this->text(),
            'sa_permanent' => $this->text(),
            'tenant_slug' => $this->string(50)->notNull(),
            
            'joining_date' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),

            'left_date' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),

            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}