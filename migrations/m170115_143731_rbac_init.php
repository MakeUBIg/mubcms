<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\migrations;

Use Yii;

use \yii\base\InvalidConfigException;
use \yii\rbac\DbManager;

/**
 * Initializes RBAC tables
 *
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since 2.0
 */
class m170115_143731_rbac_init extends \yii\db\Migration
{
    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    /**
     * @return bool
     */
    protected function isMSSQL()
    {
        return $this->db->driverName === 'mssql' || $this->db->driverName === 'sqlsrv' || $this->db->driverName === 'dblib';
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($authManager->ruleTable, [
            'name' => $this->string(64)->notNull(),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY (name)',
        ], $tableOptions);

        $this->createTable($authManager->itemTable, [
            'name' => $this->string(64)->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY (name)',
            'FOREIGN KEY (rule_name) REFERENCES ' . $authManager->ruleTable . ' (name)'.
                ($this->isMSSQL() ? '' : ' ON DELETE SET NULL ON UPDATE CASCADE'),
        ], $tableOptions);
        $this->createIndex('idx-auth_item-type', $authManager->itemTable, 'type');

        $this->createTable($authManager->itemChildTable, [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
            'PRIMARY KEY (parent, child)',
            'FOREIGN KEY (parent) REFERENCES ' . $authManager->itemTable . ' (name)'.
                ($this->isMSSQL() ? '' : ' ON DELETE CASCADE ON UPDATE CASCADE'),
            'FOREIGN KEY (child) REFERENCES ' . $authManager->itemTable . ' (name)'.
                ($this->isMSSQL() ? '' : ' ON DELETE CASCADE ON UPDATE CASCADE'),
        ], $tableOptions);

        $this->createTable($authManager->assignmentTable, [
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->string(64)->notNull(),
            'created_at' => $this->integer(),
            'PRIMARY KEY (item_name, user_id)',
            'FOREIGN KEY (item_name) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->createPermission();
        $this->createRole();
        $this->assignRole();
        $this->createDashboardRule();
        
        if($this->isMsSQL()) {
            $this->execute("CREATE TRIGGER dbo.trigger_auth_item_child
            ON dbo.{$authManager->itemTable}
            INSTEAD OF DELETE, UPDATE
            AS
            DECLARE @old_name VARCHAR (64) = (SELECT name FROM deleted)
            DECLARE @new_name VARCHAR (64) = (SELECT name FROM inserted)
            BEGIN
            IF COLUMNS_UPDATED() > 0
                BEGIN
                    IF @old_name <> @new_name
                    BEGIN
                        ALTER TABLE auth_item_child NOCHECK CONSTRAINT FK__auth_item__child;
                        UPDATE auth_item_child SET child = @new_name WHERE child = @old_name;
                    END
                UPDATE auth_item
                SET name = (SELECT name FROM inserted),
                type = (SELECT type FROM inserted),
                description = (SELECT description FROM inserted),
                rule_name = (SELECT rule_name FROM inserted),
                data = (SELECT data FROM inserted),
                created_at = (SELECT created_at FROM inserted),
                updated_at = (SELECT updated_at FROM inserted)
                WHERE name IN (SELECT name FROM deleted)
                IF @old_name <> @new_name
                    BEGIN
                        ALTER TABLE auth_item_child CHECK CONSTRAINT FK__auth_item__child;
                    END
                END
                ELSE
                    BEGIN
                        DELETE FROM dbo.{$authManager->itemChildTable} WHERE parent IN (SELECT name FROM deleted) OR child IN (SELECT name FROM deleted);
                        DELETE FROM dbo.{$authManager->itemTable} WHERE name IN (SELECT name FROM deleted);
                    END
            END;");
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        if($this->isMsSQL()) {
            $this->execute('DROP dbo.trigger_auth_item_child;');
        }

        $this->dropTable($authManager->assignmentTable);
        $this->dropTable($authManager->itemChildTable);
        $this->dropTable($authManager->itemTable);
        $this->dropTable($authManager->ruleTable);
    }
    /**
    * create permission
    * @author Praveen Kumar <praveen@makeubig.com>
    */
    protected function createPermission() 
    {
        $auth = Yii::$app->authManager;

        /**
         *              DASHBOARD PERMISSIONS
         */
        $indexd = $auth->createPermission('dashboard/index');
        $indexd->description = 'index dashboard';
        $auth->add($indexd);

        $created = $auth->createPermission('dashboard/create');
        $created->description = 'create dashboard';
        $auth->add($created);
        
        $updated = $auth->createPermission('dashboard/update');
        $updated->description = 'update dashboard';
        $auth->add($updated);
        
        $viewd = $auth->createPermission('dashboard/view');
        $viewd->description = 'view dashboard';
        $auth->add($viewd);
        
        $deleted = $auth->createPermission('dashboard/delete');
        $deleted->description = 'delete dashboard';
        $auth->add($deleted);

         /**
         *              Property PERMISSIONS
         */
        $indexp = $auth->createPermission('property/index');
        $indexp->description = 'index Property';
        $auth->add($indexp);

        $createp = $auth->createPermission('property/create');
        $createp->description = 'create Property';
        $auth->add($createp);
        
        $updatep = $auth->createPermission('property/update');
        $updatep->description = 'update Property';
        $auth->add($updatep);
        
        $viewp = $auth->createPermission('property/view');
        $viewp->description = 'view Property';
        $auth->add($viewp);
        
        $deleteg = $auth->createPermission('property/delete');
        $deleteg->description = 'delete Property';
        $auth->add($deleteg);


        $indexro = $auth->createPermission('room/index');
        $indexro->description = 'index Property room';
        $auth->add($indexro);

        $creatro = $auth->createPermission('room/create');
        $creatro->description = 'create Property room';
        $auth->add($creatro);
        
        $updatero = $auth->createPermission('room/update');
        $updatero->description = 'update Property room';
        $auth->add($updatero);
        
        $viewro = $auth->createPermission('room/view');
        $viewro->description = 'view Property room';
        $auth->add($viewro);

        $deletero = $auth->createPermission('room/delete');
        $deletero->description = 'delete Property room';
        $auth->add($deletero);


        $indexbe = $auth->createPermission('bed/index');
        $indexbe->description = 'index Property bed';
        $auth->add($indexbe);

        $createbe = $auth->createPermission('bed/create');
        $createbe->description = 'create Property bed';
        $auth->add($createbe);
        
        $updatebe = $auth->createPermission('bed/update');
        $updatebe->description = 'update Property bed';
        $auth->add($updatebe);
        
        $viewbe = $auth->createPermission('bed/view');
        $viewbe->description = 'view Property bed';
        $auth->add($viewbe);
        
        $deletebe = $auth->createPermission('bed/delete');
        $deletebe->description = 'delete Property bed';
        $auth->add($deletebe);

        /*
                    Amenities permission
        */ 
        $indexAmenity = $auth->createPermission('amenity/index');
        $indexAmenity->description = 'index amenity';
        $auth->add($indexAmenity);

        $createAmenity = $auth->createPermission('amenity/create');
        $createAmenity->description = 'create amenity';
        $auth->add($createAmenity);
        
        $updateAmenity = $auth->createPermission('amenity/update');
        $updateAmenity->description = 'update amenity';
        $auth->add($updateAmenity);
        
        $viewAmenity = $auth->createPermission('amenity/view');
        $viewAmenity->description = 'view amenity';
        $auth->add($viewAmenity);
        
        $deleteAmenity = $auth->createPermission('amenity/delete');
        $deleteAmenity->description = 'delete amenity';
        $auth->add($deleteAmenity);

        /*
                    Tenant permission
        */ 
        $indexTenant = $auth->createPermission('tenant/index');
        $indexTenant->description = 'index tenant';
        $auth->add($indexTenant);

        $createTenant = $auth->createPermission('tenant/create');
        $createTenant->description = 'create tenant';
        $auth->add($createTenant);
        
        $updateTenant = $auth->createPermission('tenant/update');
        $updateTenant->description = 'update tenant';
        $auth->add($updateTenant);
        
        $viewTenant = $auth->createPermission('tenant/view');
        $viewTenant->description = 'view tenant';
        $auth->add($viewTenant);
        
        $deleteTenant = $auth->createPermission('tenant/delete');
        $deleteTenant->description = 'delete tenant';
        $auth->add($deleteTenant);

        /*
                    Support Staff permission
        */ 
        $indexSupportStaff = $auth->createPermission('support-staff/index');
        $indexSupportStaff->description = 'index support-staff';
        $auth->add($indexSupportStaff);

        $createSupportStaff = $auth->createPermission('support-staff/create');
        $createSupportStaff->description = 'create support-staff';
        $auth->add($createSupportStaff);
        
        $updateSupportStaff = $auth->createPermission('support-staff/update');
        $updateSupportStaff->description = 'update support-staff';
        $auth->add($updateSupportStaff);
        
        $viewSupportStaff = $auth->createPermission('support-staff/view');
        $viewSupportStaff->description = 'view support-staff';
        $auth->add($viewSupportStaff);
        
        $deleteSupportStaff = $auth->createPermission('support-staff/delete');
        $deleteSupportStaff->description = 'delete support-staff';
        $auth->add($deleteSupportStaff);

        
        /**
         *              User PERMISSIONS
         */
        $indexu = $auth->createPermission('user/index');
        $indexu->description = 'index user';
        $auth->add($indexu);

        $createu = $auth->createPermission('user/create');
        $createu->description = 'create user';
        $auth->add($createu);
        
        $updateu = $auth->createPermission('user/update');
        $updateu->description = 'update user';
        $auth->add($updateu);
        
        $viewu = $auth->createPermission('user/view');
        $viewu->description = 'view user';
        $auth->add($viewu);
        
        $deleteu = $auth->createPermission('user/delete');
        $deleteu->description = 'delete user';
        $auth->add($deleteu);

        $indexa = $auth->createPermission('auth');
        $indexa->description = 'Auth Access';
        $auth->add($indexa);
    }

    /**
    * create role
    * @author Praveen Kumar <praveen@makeubig.com>
    */
    protected function createRole() 
    {
        $auth = Yii::$app->authManager;
        //admin -> Own view/update
        //superadmin -> (admin) and -> view/update/create/index/delete
        
        $indexd = $auth->createPermission('dashboard/index');
        $created = $auth->createPermission('dashboard/create');
        $updated = $auth->createPermission('dashboard/update');
        $viewd = $auth->createPermission('dashboard/view');
        $deleted = $auth->createPermission('dashboard/delete');
        
        $indexu = $auth->createPermission('user/index');
        $createu = $auth->createPermission('user/create');
        $updateu = $auth->createPermission('user/update');
        $viewu = $auth->createPermission('user/view');
        $deleteu = $auth->createPermission('user/delete');
        
        $indexp = $auth->createPermission('property/index');
        $createp = $auth->createPermission('property/create');
        $updatep = $auth->createPermission('property/update');
        $viewp = $auth->createPermission('property/view');
        $deletep = $auth->createPermission('property/delete');

        $indexro = $auth->createPermission('room/index');
        $creatro = $auth->createPermission('room/create');
        $updatero = $auth->createPermission('room/update');
        $viewro = $auth->createPermission('room/view');
        $deletero = $auth->createPermission('room/delete');

        $indexbe = $auth->createPermission('bed/index');
        $createbe = $auth->createPermission('bed/create');
        $updatebe = $auth->createPermission('bed/update');
        $viewbe = $auth->createPermission('bed/view');
        $deletebe = $auth->createPermission('bed/delete');

        $indexAmenity = $auth->createPermission('amenity/index');
        $createAmenity = $auth->createPermission('amenity/create');
        $updateAmenity  = $auth->createPermission('amenity/update');
        $deleteAmenity = $auth->createPermission('amenity/delete');
        $viewAmenity = $auth->createPermission('amenity/view');

        $indexTenant = $auth->createPermission('tenant/index');
        $createTenant = $auth->createPermission('tenant/create');
        $updateTenant  = $auth->createPermission('tenant/update');
        $deleteTenant = $auth->createPermission('tenant/delete');
        $viewTenant = $auth->createPermission('tenant/view');

        $indexSupportStaff = $auth->createPermission('support-staff/index');
        $createSupportStaff = $auth->createPermission('support-staff/create');
        $updateSupportStaff = $auth->createPermission('support-staff/update');
        $deleteSupportStaff = $auth->createPermission('support-staff/delete');
        $viewSupportStaff = $auth->createPermission('support-staff/view');

        $indexa = $auth->createPermission('auth');  

        $subadmin = $auth->createRole('subadmin');
        $auth->add($subadmin);
        $admin = $auth->createRole('admin');
        $client = $auth->createRole('client');
        $auth->add($admin);
        $auth->add($client);
        $auth->addChild($admin, $subadmin);
        $auth->addChild($admin, $indexa);
        $auth->addChild($admin, $client);
        
        $superadmin = $auth->createRole('mubadmin');
        $auth->add($superadmin);
        $auth->addChild($superadmin, $admin);
        $auth->addChild($superadmin, $indexd);
        $auth->addChild($superadmin, $created);
        $auth->addChild($superadmin, $deleted);
        $auth->addChild($superadmin, $updated);
        $auth->addChild($superadmin, $viewd);
        $auth->addChild($superadmin, $indexu);
        $auth->addChild($superadmin, $createu);
        $auth->addChild($superadmin, $deleteu);
        $auth->addChild($superadmin, $updateu);
        $auth->addChild($superadmin, $viewu);
        $auth->addChild($superadmin, $indexp);
        $auth->addChild($superadmin, $createp);
        $auth->addChild($superadmin, $deletep);
        $auth->addChild($superadmin, $updatep);
        $auth->addChild($superadmin, $viewp);
        $auth->addChild($superadmin, $indexbe);
        $auth->addChild($superadmin, $createbe);
        $auth->addChild($superadmin, $deletebe);
        $auth->addChild($superadmin, $updatebe);
        $auth->addChild($superadmin, $viewbe);
        $auth->addChild($superadmin, $indexro);
        $auth->addChild($superadmin, $creatro);
        $auth->addChild($superadmin, $deletero);
        $auth->addChild($superadmin, $updatero);
        $auth->addChild($superadmin, $viewro);
        $auth->addChild($superadmin, $indexAmenity);
        $auth->addChild($superadmin, $createAmenity);
        $auth->addChild($superadmin, $deleteAmenity);
        $auth->addChild($superadmin, $updateAmenity);
        $auth->addChild($superadmin, $viewAmenity);
        $auth->addChild($superadmin, $indexTenant);
        $auth->addChild($superadmin, $createTenant);
        $auth->addChild($superadmin, $deleteTenant);
        $auth->addChild($superadmin, $updateTenant);
        $auth->addChild($superadmin, $viewTenant);
        $auth->addChild($superadmin, $indexSupportStaff);
        $auth->addChild($superadmin, $createSupportStaff);
        $auth->addChild($superadmin, $deleteSupportStaff);
        $auth->addChild($superadmin, $updateSupportStaff);
        $auth->addChild($superadmin, $viewSupportStaff);

        $auth->addChild($admin, $indexd);
        $auth->addChild($admin, $created);
        $auth->addChild($admin, $deleted);
        $auth->addChild($admin, $updated);
        $auth->addChild($admin, $viewd);
        $auth->addChild($admin, $indexp);
        $auth->addChild($admin, $createp);
        $auth->addChild($admin, $deletep);
        $auth->addChild($admin, $updatep);
        $auth->addChild($admin, $viewp);
        $auth->addChild($admin, $indexbe);
        $auth->addChild($admin, $createbe);
        $auth->addChild($admin, $deletebe);
        $auth->addChild($admin, $updatebe);
        $auth->addChild($admin, $viewbe);
        $auth->addChild($admin, $indexro);
        $auth->addChild($admin, $creatro);
        $auth->addChild($admin, $deletero);
        $auth->addChild($admin, $updatero);
        $auth->addChild($admin, $viewro);
        $auth->addChild($admin, $indexu);
        $auth->addChild($admin, $createu);
        $auth->addChild($admin, $deleteu);
        $auth->addChild($admin, $updateu);
        $auth->addChild($admin, $viewu);
        $auth->addChild($admin, $indexAmenity);
        $auth->addChild($admin, $createAmenity);
        $auth->addChild($admin, $deleteAmenity);
        $auth->addChild($admin, $updateAmenity);
        $auth->addChild($admin, $viewAmenity);
        $auth->addChild($admin, $indexTenant);
        $auth->addChild($admin, $createTenant);
        $auth->addChild($admin, $deleteTenant);
        $auth->addChild($admin, $updateTenant);
        $auth->addChild($admin, $viewTenant);
        $auth->addChild($admin, $indexSupportStaff);
        $auth->addChild($admin, $createSupportStaff);
        $auth->addChild($admin, $deleteSupportStaff);
        $auth->addChild($admin, $updateSupportStaff);
        $auth->addChild($admin, $viewSupportStaff);

        $auth->addChild($subadmin, $indexd);
        $auth->addChild($subadmin, $created);
        $auth->addChild($subadmin, $deleted);
        $auth->addChild($subadmin, $updated);
        $auth->addChild($subadmin, $viewd);
        $auth->addChild($subadmin, $indexp);
        $auth->addChild($subadmin, $createp);
        $auth->addChild($subadmin, $updatep);
        $auth->addChild($subadmin, $viewp);
        $auth->addChild($subadmin, $indexbe);
        $auth->addChild($subadmin, $createbe);
        $auth->addChild($subadmin, $updatebe);
        $auth->addChild($subadmin, $viewbe);
        $auth->addChild($subadmin, $indexro);
        $auth->addChild($subadmin, $creatro);
        $auth->addChild($subadmin, $updatero);
        $auth->addChild($subadmin, $viewro);
        $auth->addChild($subadmin, $indexTenant);
        $auth->addChild($subadmin, $createTenant);
        $auth->addChild($subadmin, $deleteTenant);
        $auth->addChild($subadmin, $updateTenant);
        $auth->addChild($subadmin, $viewTenant);
        $auth->addChild($subadmin, $indexSupportStaff);
        $auth->addChild($subadmin, $createSupportStaff);
        $auth->addChild($subadmin, $deleteSupportStaff);
        $auth->addChild($subadmin, $updateSupportStaff);
        $auth->addChild($subadmin, $viewSupportStaff);
    }
    /**
    * create user rule
    * @author Praveen Kumar <praveen@makeubig.com>
    */
    protected function createDashboardRule() 
    {
        $auth = Yii::$app->authManager;
        // add the rule
        $rule = new \app\components\rbac\AdminUserRule();
        $auth->add($rule);

        // add the "updateOwnUser" permission and associate the rule with it.
        $updateOwnUser = $auth->createPermission('updateOwnUser');
        $updateOwnUser->description = 'Update own User';
        $updateOwnUser->ruleName = $rule->name;
        $auth->add($updateOwnUser);

        $updatePost = $auth->createPermission('dashboard/update');
        // "updateOwnPost" will be used from "updatePost"
        $auth->addChild($updateOwnUser, $updatePost);

        $admin = $auth->createPermission('admin');
        // allow "author" to update their own posts
        $auth->addChild($admin, $updateOwnUser);
        
        // add the "viewOwnuser" permission and associate the rule with it.
        $viewOwnUser = $auth->createPermission('viewOwnUser');
        $viewOwnUser->description = 'View own User';
        $viewOwnUser->ruleName = $rule->name;
        $auth->add($viewOwnUser);

        $viewPost = $auth->createPermission('dashboard/view');
        // "updateOwnPost" will be used from "updatePost"
        $auth->addChild($viewOwnUser, $viewPost);
        $auth->addChild($admin, $viewOwnUser);
    }

    /**
    * assign role
    * @author Praveen Kumar <praveen@makeubig.com>
    */
    protected function assignRole()
    {
        $auth = Yii::$app->authManager;
        
        $superadmin = $auth->createRole('mubadmin');
        $auth->assign($superadmin, 1);
        $admin = $auth->createRole('admin');
        $auth->assign($admin, 2);
        $subadmin = $auth->createRole('subadmin');
        $user = \app\models\User::find()->all();
        foreach ($user as $key => $value) 
        {
            if($value->id > 2)
            {
                $auth->assign($subadmin, $value->id);
            }
        }
    }
}