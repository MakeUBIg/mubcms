<?php

namespace app\migrations;
use app\commands\Migration;

class m170430_055658_create_property extends Migration
{
    public function getTableName()
    {
        return 'property';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'state_id' => ['state','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'property_name' => 'property_name',
            'property_slug'  =>  'property_slug',
            'city_name' => 'city_name'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'rooms_count' => $this->integer()->defaultValue('0'),
            'locality_name' => $this->string(),
            'lat' => $this->string(),
            'long' => $this->string(),
            'city_name' => $this->string(),
            'state_id' => $this->integer()->notNull(),
            'property_name' => $this->string()->notNull(),
            'property_slug' => $this->string()->notNull(),
            'property_type' => "enum('hostel','pg','flat','house') NOT NULL DEFAULT 'pg'",
            'property_category' => "enum('economy','flagship','premium') NOT NULL DEFAULT 'flagship'",
            'property_area' => "enum('boys','girls','both','family') NOT NULL DEFAULT 'boys'",
            'sa_a' => $this->string()->notNull(),
            'sa_b' => $this->string()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'description' => $this->text(),
            'pincode' => $this->integer(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
