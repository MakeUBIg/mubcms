<?php

namespace app\migrations;
use app\commands\Migration;

class m170430_055717_create_bed extends Migration
{
    public function getTableName()
    {
        return 'bed';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'room_id' => ['room','id']
        ];
    }


    public function getKeyFields()
    {
        return [
            'bed_number' => 'bed_number',
            'available'  =>  'available',
            'price' => 'price'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'room_id' => $this->integer()->notNull(),
            'bed_number' => $this->integer(),
            'available' => "enum('0','1') NOT NULL DEFAULT '0'",
            'last_booked' => $this->dateTime(),
            'availability_date' => $this->dateTime(),
            'type' => "enum('single','double') NOT NULL DEFAULT 'single'",
            'price' => $this->integer()->notNull(),
            'extrafield1' => $this->string(),
            'extrafield2' => $this->string(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
