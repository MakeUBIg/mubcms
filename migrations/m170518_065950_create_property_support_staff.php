<?php
namespace app\migrations;
use app\commands\Migration;


class m170518_065950_create_property_support_staff extends Migration
{
    public function getTableName()
    {
        return 'property_support_staff';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'property_id' => ['property','id'],
        ];
    }

    public function getKeyFields()
    {
        return [
                'email' => 'email',
                'mobile' => 'mobile',
                'joining_date' => 'joining_date',
                'designation' => 'designation'
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'property_id' => $this->integer(),

            'staff_name' => $this->string(50)->notNull(),
            'email' => $this->string(50),

            'mobile' => $this->integer(),
            'state_id' => $this->integer(),
            'support_staff_slug' => $this->string(50)->notNull(),
            'city_name' => $this->string(),
            'salary' => $this->integer(),
            'designation' => $this->string(),
            'experience' => $this->string(),
            'visite_charge' => $this->integer(),
            'type' => "enum('regular','professional') NOT NULL DEFAULT 'regular'",

            'joining_date' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'left_date' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),

            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
