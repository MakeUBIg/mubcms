<?php

namespace app\migrations;
use app\commands\Migration;

class m170115_144829_create_mub_user_images extends Migration
{
    public function getTableName()
    {
        return 'mub_user_images';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'title' => 'title',
            'url' => 'url',
            'keyword' => 'keyword',
            'album_id' => 'album_id',
            'mub_user_id'  =>  'mub_user_id',
            'visible' => 'visible',
            'width' => 'width',
            'height' =>'height',
            'full_path' => 'full_path',
            'thumbnail_path' => 'thumbnail_path',
            'thumbnail_url' => 'thumbnail_url'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'album_id' => $this->integer()->notNull()->defaultValue('1'),
            'title' => $this->string(50)->notNull(),
            'description' => $this->string(100)->notNull(),
            'url' => $this->string(100)->notNull(),
            'thumbnail_url' => $this->string(100)->notNull(),
            'thumbnail_path' => $this->string(100),
            'full_path' => $this->string(100)->notNull(),
            'visible' => "enum('0','1')",
            'keyword' => $this->string(),
            'width' => $this->integer(),
            'height' => $this->integer(),
            'display_width' => $this->integer(),
            'display_hieght' => $this->integer(),
            'type' => "enum('original','medium','thumbnail') NOT NULL DEFAULT 'original'",
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
