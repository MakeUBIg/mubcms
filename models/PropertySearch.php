<?php
namespace app\models;
use yii\data\Pagination;
use app\components\Model;
use Yii;

class PropertySearch extends Model
{
    public function isStateName($location)
    {
        $stateLocations = 
        [
            'Delhi, India',
            'New Delhi, Delhi, India',
            'Haryana, India'
        ];
        if(in_array($location, $stateLocations))
        {
            return true;
        }
        return false;
    }
    public function isCityName($location)
    {
       $places = explode(', ', $location);
       if(count($places) == '3')
       {
            return true;
       }
       return false;
    }

	public function getPropertyList($searchData)
	{
        if($searchData['current_state'])
        {
        
        $fields = [
            'property.id',
            'property.property_name',
            'property.city_name',                
            'property.mub_user_id','property.property_type',
            'property.property_category',
            'property.property_area',
            'property.sa_a',
            'property.description',
            'property.sa_b',
            'ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( '.$searchData['lat'].' ) ) + COS( RADIANS( `lat` ) )* COS( RADIANS('.$searchData['lat'].' )) * COS( RADIANS( `long` ) - RADIANS( '.$searchData['long'].' )) ) * 6371 AS `distance`'
            ];
        //if no preference for property type show price will be lowest room price
        if(!isset($searchData['rooms_type']) || $searchData['rooms_type'] == '')
        {
            $fields[] = 'MIN(bed.price) as show_price';   
        }
        elseif(isset($searchData['rooms_type']) && $searchData['rooms_type'] == 'house')
        {
            $fields[] = 'MIN(room.price) as show_price_min';
            $fields[] = 'MAX(room.price) as show_price_max';
            $fields[] = 'MIN(room.price) as show_price';  
        }
        else
        {
            $fields[] = 'MIN(bed.price) as show_price_min'; 
            $fields[] = 'MAX(bed.price) as show_price_max'; 
            $fields[] = 'MIN(bed.price) as show_price'; 
        }

        $stateId = $searchData['current_state']->id;
        $property = new \app\modules\MubAdmin\modules\RealEstate\models\Property();
        $connection = Yii::$app->getDb();

        //if the user has entered a state name
        if($this->isStateName($searchData['location']))
        {
            //if search is a state name
            $query = $property::find()->select($fields)->where(['state_id' => $stateId,'property.del_status' => '0','property.status' => 'active']);         
        }
        elseif($this->isCityName($searchData['location']))
        {
            $places = explode(', ', $searchData['location']);
            $cityName = $places[0];
            if($cityName == 'Gurgaon')
            {
                $cityName = 'Gurugram';
            }
            $query = $property::find()->select($fields)->where(['city_name' => $cityName,'property.del_status' => '0','property.status' => 'active']);   
        }
        else //user entered sublocation
        {
        $query = $property::find()->select($fields)->where('ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( '.$searchData['lat'].' ) ) + COS( RADIANS( `lat` ) ) * COS( RADIANS( '.$searchData['lat'].' )) * COS( RADIANS( `long` ) - RADIANS( '.$searchData['long'].' )) ) * 6371 < .1 AND property.del_status = "0" AND property.status = "active"');
        }

        $query->innerJoin('room','property.id = room.property_id')->andWhere(['room.del_status' => '0']);
        
        //by default search result will display results for shared property type
        if(!isset($searchData['rooms_type']) || $searchData['rooms_type'] != 'house')
        {
            $query->leftJoin('bed','room.id = bed.room_id')->andWhere(['bed.del_status' => '0','bed.available' => '1']);
        }

        $query = $this->getFileteredResult($query,$searchData);

        //if no result found for shared property type and there is no prefernce over property type
        if($query->count() == '0')
        {
            if(!isset($searchData['rooms_type']) || $searchData['rooms_type'] == '')
            {
               $searchData['rooms_type'] = 'house';
               return $this->getPropertyList($searchData);
            }
        }
        $queryCount = $query->count();
        $pages = new Pagination(['totalCount' => $query->count(), 'defaultPageSize' => 12]);
        $result = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();

        $searchData['result'] = $result;
        $searchData['result_count'] = $queryCount;
        $searchData['pages'] = $pages;
        return $searchData;   
        }
        return false;
    }

    public function getFileteredResult($query,$params)
    {
        $allCategory = [];
        $allFurnishing = [];
        if(!empty($params['rooms_type']))
        {
            $query->andWhere(['property_type' => $params['rooms_type']]);
        }
        if(!empty($params['rooms_for']))
        {
            $query->andWhere(['property_area' => $params['rooms_for']]);
        }
        if(!empty($params['tag_premium']))
        {
            $allCategory[] = 'premium';
        }
        if(!empty($params['tag_flagship']))
        {
            $allCategory[] = 'flagship';
        }
        if(!empty($params['tag_economy']))
        {
            $allCategory[] = 'economy';
        }
        if(!empty($params['fullyfur']))
        {
            $allFurnishing[] = 'furnished';
        }
        if(!empty($params['semifur']))
        {
            $allFurnishing[] = 'semifurnished';
        }
        if(!empty($params['unfur']))
        {
            $allFurnishing[] = 'unfurnished';
        }
        if(!empty($params['uplimit']) && !empty($params['lowlimit']))
        {
            $query->having(['between','show_price',$params['lowlimit'],$params['uplimit']]);
        }
        if(!empty($params['sort']) && !$this->isStateName($params['location']))
        {
            switch($params['sort'])
            {
                case 'dlth': 
                {
                    $query->orderBy(['distance' => SORT_ASC]);
                }
                break;
                case 'dhtl': 
                {
                    $query->orderBy(['distance' => SORT_DESC]);
                }
                break;
                case 'plth': 
                {
                    $query->orderBy(['show_price' => SORT_ASC]);

                }
                break;
                case 'phtl': 
                {
                    $query->orderBy(['show_price' => SORT_DESC]);
                }
                break;
            }
        }
        else
        {
            if(!$this->isStateName($params['location']))
            {
                $query->orderBy(['show_price' => SORT_ASC]);
            }
        }
        $query->andWhere(['property_category' => $allCategory]);
        $query->andWhere(['room.room_type' => $allFurnishing]);
        $query->groupBy(['property.id']);
        // p($query->createCommand()->rawSql);
        return $query;
    }

    public function getSimilarProperty($property)
    {
         $fields = [
                'property.id',
                'property.property_name',
                'property.city_name',                
                'property.mub_user_id','property.property_type',
                'property.property_category',
                'property.property_area',
                'property.sa_a',
                'property.sa_b',
                'ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( '.$property->lat.' ) ) + COS( RADIANS( `lat` ) )* COS( RADIANS('.$property->lat.' )) * COS( RADIANS( `long` ) - RADIANS( '.$property->long.' )) ) * 6371 AS `distance`'
                ];

                if($property->property_type == 'house')
                {
                    $fields[] = 'MIN(room.price) as show_price';  
                }
                else
                {
                    $fields[] = 'MIN(bed.price) as show_price';   
                }
        $query = $property::find()->select($fields)->where('ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( '.$property->lat.' ) ) + COS( RADIANS( `lat` ) ) * COS( RADIANS( '.$property->lat.' )) * COS( RADIANS( `long` ) - RADIANS( '.$property->long.' )) ) * 6371 < 10 AND property.del_status = "0" AND property.status = "active"');
        $query->innerJoin('room','property.id = room.property_id')->andWhere(['room.del_status' => '0']);
        if($property->property_type != 'house')
        {
            $query->leftJoin('bed','room.id = bed.room_id')->andWhere(['bed.del_status' => '0','bed.available' => '1']);
        }
        $query->andWhere(['<>','property.id',$property->id]);
        $query->andWhere(['property.property_type' => $property->property_type])->limit(3)->groupBy(['property.id']);
        return $query->all();
    }
}
