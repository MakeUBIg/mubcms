   <style type="text/css">
       .modal-dialog
       {
        width: 65%!important;
       }
   </style>
    <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" style="margin-bottom: 1.1em;">&times;</button>
            <h4 class="modal-title text-center"><?=$property->property_name . '&nbsp-&nbspAll Beds';?></h4>
    </div>
      <div class="modal-body" style="margin-top: -3em!important;">
        <?php  if(is_array($property->rooms))
        {
            foreach ($property->rooms as $roomKey => $room)
                {
                    $colCount = count($room->beds);
                    ?>
                            
                <?php if(!empty($room->beds)){?>
                
                <div class="col-md-4 room text-center">
                <div class="col-md-12 text-center">
                    <h3 class="margin-general"><?= $room->room_name;?></h3>
                </div>
                    <div class="col-md-12" style="background-color: #EEEEEE; border-radius: 10px;">
                        <?php foreach($room->beds as $bed){
                            $colSize = \app\helpers\HtmlHelper::getColSize($colCount);
                            ?>
                            <div class="col-md-<?=$colSize;?> <?= ($bed->available) ? '' : 'inactive';?> text-center">
                                <div class="row">
                                    <?= 'Bed '. $bed->bed_number;?>    
                                </div>
                                <div class="tool">
                                <span >
                                    <i class="fa fa-bed" aria-hidden="true" style="font-size:2.5em;margin-top: 0.3em; margin-bottom: 0.2em;"></i>
                                </span> 
                                 <?= ($bed->available) ? '<span class="tooltext">Available</span>' : '<span class="tooltext back">Occupied</span>';?> 
                                </div>
                                <div class="row">
                                    ₹<?= $bed->price;?>                                    
                                </div>
                            </div>
                        <?php }?>    
                    </div> 
                    </div>               
                <?php }?>
        <?php }}else{?>
                <div class="col-md-12 text-center"><h3>Sorry there is no Rooms added in this Property</h3></div>
                <?php }?>
      </div>
      <div class="modal-footer">
      </div>