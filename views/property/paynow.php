<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>

$(document).ready(function(){
    $("#booking-price").keydown(function(e){
        var price = $("#booking-price").val();
        $.ajax('');
    });
    $("#booking-tax").keyup(function(e){
         var tax = $("#booking-tax").val();
         $.ajax('');
    });
});

</script>

<div class="modal-header">
    <div class="row container-fluid">
        <div class="pull-right"><button style="display:inline" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    </div>                        
</div>
<div class="modal-body real-spa">
<div class="login-grids">
    <div class="login">
        <div class="login-right">
        <?php $form = ActiveForm::begin(['layout' => 'horizontal','options' => ['id' => 'paynow','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
               <div class="col-md-12 text-center" style="margin-bottom: 1em; margin-top: -2em"><h2>Pay Amount</h2></div>
                <?= $form->field($model, 'price')->textInput() ?>

                 <?= $form->field($model, 'tax')->textInput() ?>

                <input type="submit" value="Login" >
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>
<div class="modal-footer">  
</div>
