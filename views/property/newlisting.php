<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\HomeListing;
?>
<style>
    .help-block
    {
        font-size:10px;
    }
    .modal-dialog
    {
     width: 65%!important;
     }
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
<div class="col-md-12 text-center" style="margin-bottom: -2em;"><h2>List Your Homes</h2></div>
</div>
<div class="modal-body real-spa" style=" margin-bottom: 1.4em;">
    <?php $form = ActiveForm::begin(['options' => ['id' => 'list_homes','method' => 'POST','data-pjax' => true],'action' => ['/']]);?>
    <div class="row">
    <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($homeListing, 'property_type')->dropDownList([ 'hostel' => 'Hostel', 'pg' => 'PG', 'flat' => 'Flat','house' => 'House' ], ['prompt' => 'Select Property Type']) ?>
    </div>
    <div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($homeListing, 'property_area')->dropDownList([ 'boys' => 'Boys', 'girls' => 'Girls', 'both' => 'Both', 'family' => 'Family', ], ['prompt' => 'Select Property For'])->label('Property For') ?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($homeListing, 'owner_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($homeListing, 'mobile')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($homeListing, 'email')->textInput(['maxlength' => true ,'class' => 'form-control']) ?>
    </div>
    <div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($homeListing, 'sa_a')->textInput(['maxlength' => true,'placeholder' => 'Like plot Number/Khasra number'])->label('Street address Line1');?>
    </div>
    </div>
    <div class="col-md-12 text-center" >
        <input type="submit" class="btn btn-success" value="Submit"></input>        
    </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div class="modal-footer">  
</div>