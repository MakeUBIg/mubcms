<style type="text/css">
       .modal-dialog
       {
        width: 65%!important;
       }

        </style>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title text-center"><?=$property->property_name . '&nbsp-&nbspAll Rooms';?></h4>
</div>
<div class="modal-body">
  <?php
    if(is_array($property->rooms))
    {
      $roomCount = count($property->rooms);
      if($roomCount > 1)
      {?>
      <div class="company-grids text-center">
          <?php foreach($property->rooms as $roomKey => $room)
          { $allRooms = $property->rooms;
            $roomCount = count($allRooms);
            $roomCount = intval(count($property->rooms));
                    $unAvailableBeds = count($room->unavailableBed);
                    $colSize = \app\helpers\HtmlHelper::getColSize($roomCount);
                    $colClass = ($roomCount > 7) ? "col-md-2" : "col-md-".$colSize;
                    ?>
            <div class="col-md-2">
            <div class="company-grid multi-gd-text <?= (intval($unAvailableBeds) > 0) ? 'inactive' : '';?>"> 
            <div class="row">
                  <?=$room->room_name;?>
                  </div>
            <div class="tool">
            <img src="/images/room.png" height="60" width="60" style=" margin-top: 0.3em; margin-bottom: 0.2em; margin-left: -.8em;"><br />
            <span class="tooltext"><?= (intval($unAvailableBeds) > 0) ? '<span class="tooltext back" style="margin-bottom:0.5em!important;">Occupied</span>' : 'Available';?></span>
            </div>
            <div class="row">
            ₹<?= $room->price;?>
            </div>
          </div></div>
          <?php }?>
          <div class="clearfix"></div>
       </div>
      <?php }else{
    foreach ($property->rooms as $roomKey => $room)
    {
    $allRooms = $property->rooms;
    $roomCount = count($allRooms);
    $colSize = \app\helpers\HtmlHelper::getColSize($roomCount);?>
    <div class="col-md-<?=$colSize;?> text-center ">
    <div class="row">
       <?=$room->room_name;?>
    </div>
    <div class="row">
       <i class="fa fa-home tooltipstered" title = "I am working" aria-hidden="true" style="font-size:3em; margin-top: 0.3em; margin-bottom: 0.2em;"></i><br />
    </div>
    <div class="row">
      ₹  <?= $room->price;?>
    </div>
 </div>
 <?php }}}?>
</div>
<div class="modal-footer">
      </div>
