<?php 
   use yii\helpers\Html;
   use \app\helpers\ImageUploader;
   use \app\models\ScheduledVisits;
   use \app\models\Booking;
   use yii\bootstrap\ActiveForm;
   use app\helpers\TaxHelper;
   use \app\modules\MubAdmin\modules\RealEstate\models\Amenity;
   use \app\modules\MubAdmin\modules\RealEstate\models\PropertyAmenities;
   $this->registerCssFile("/css/bootstrap-datetimepicker.min.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
   $this->title = strtoupper($property->property_type) . ' in '. $property->sa_b;
   $this->registerCssFile("/css/font-awesome.min.css");
    $tokenAmt = ($property->rooms[0]->beds) ? ceil(($property->rooms[0]->beds[0]->price)/2) : '3000';

    $ogImage = (empty($property->propertyImages)) ? "http://".$_SERVER['HTTP_HOST']."/uploads/logo.jpg" : "http://".$_SERVER['HTTP_HOST'].$property->propertyImages[0]->url;

    $ogdesc = substr(str_replace('&nbsp;', ' ', strip_tags($property->description)) ,0 , 80).' ...';

$this->registerMetaTag([
      'title' => 'og:title',
      'content' => $property->property_name.' , '. $property->sa_b, 
   ]);

   $this->registerMetaTag([
      'app_id' => 'fb:app_id',
      'content' => '892362904261944'
   ]);

   $this->registerMetaTag([
      'type' => 'og:type',
      'content' => 'article'
   ]);

  $this->registerMetaTag([
      'type' => 'og:keyword',
      'content' => 'Property for Rent in Gurugram, Rent a property in Gurgoan, Apartment for Rent in Gurgoan, Flats for Rent in Gurgoan, Independent Rooms in Gurgoan, Room Sharing Gurgoan '
   ]);

   $this->registerMetaTag([
      'url' => 'og:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
      $this->registerMetaTag([
         'image' => 'og:image',
         'content' => $ogImage,
      ]);
   $this->registerMetaTag([
      'description' => 'og:description',
      'content' => $ogdesc
   ]);

/*close facebook meta tag*/

/*twitter meta tag*/

   $this->registerMetaTag([
      'card' => 'twitter:card',
      'content' => "summury"
   ]);
   $this->registerMetaTag([
      'site' => "twitter:site",
      'content' => "@publisher_handle"
   ]);
   
   $this->registerMetaTag([
      'title' => 'twitter:title',
      'content' => $property->property_name.' , '. $property->sa_b
   ]);

   $this->registerMetaTag([
      'description' => 'twitter:description',
      'content' => $ogdesc 
   ]);

   $this->registerMetaTag([
      'creater' => 'twitter:creater',
      'content' => '@author_handle' 
   ]);

   $this->registerMetaTag([
      'url' => 'twitter:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
   $this->registerMetaTag([
         'image' => 'twitter:image:src',
         'content' => $ogImage,
      ]);
    
   ?>
   <style type="text/css">
     @media (min-width: 992px){
  .col-md-8 {
    width: 63.666667%!important;
      }
      .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    color: #fff!important;
    cursor: default!important;
    background-color: #f7882f!important;
    border: 1px solid #ddd!important;
    border-bottom-color: transparent!important;
    }
    }
    .form-horizontal .control-label {
      width: 135px!important;
    }
   @media (max-width: 468px)
  {
  .margleft {margin-top: 3em!important;}
.carousel-inner{
  margin-bottom: 1em!important;
  }
.val2
{
  margin-left: 1em!important;
  }
.about-grid{width:97%!important; margin-left: 0.3em;}
}

}
</style>
<?php 
  if(!Yii::$app->user->isGuest)
  {
     $mubUserId = \app\models\User::getMubUserId();
     $mubUserModel = new \app\models\MubUser();
     $currentUser = $mubUserModel::findOne($mubUserId);
     $userDetails = $currentUser;
     $contactDetails = $currentUser->mubUserContacts;
  }
  ?>
<div class="content">
        <div class="col-xs-12 col-md-8 col-md-offset-1 margleft val1">
         <div class="pull-left" style="margin-bottom: -2em; margin-top: 1em; margin-left: -1em" >
         <h2 class="tittle"><?= ucwords($property->property_name) . ' , '.$property->sa_b;?></h2>
         </div>
         </div>
         <div class="col-xs-12 col-md-8 col-md-offset-1 val2">
            <div class="pull-left" style="margin-bottom: 1em;margin-top: 0.5em; margin-left: -1em; ">
               <p><i class="fa fa-home"></i> > Search Result > Property Details</p>
            </div>
         </div>
   <div class="about-mubs">
      <div class="container">
         <div class="row">
            <div class="col-md-8 about-grid-1">
               <div id="myCarousel" class="carousel slide" data-interal="2000" data-ride="carousel">
                  <div class="carousel-inner">
                  <?php
                  if(!empty($property->propertyImages)){
                   foreach($property->propertyImages as $index => $image){?>
                     <div class="item <?= ($index== '0') ? 'active' : ''?>">
                        <picture>
                           <?php echo Html::img(ImageUploader::resizeRender($image->url, '640', '425'),['class' => 'img-responsive','alt' => $property->sa_b]);?>
                        </picture>
                     </div>
                     <?php }}else{?>
                      <div class="item active">
                        <picture>
                           <?php echo Html::img(ImageUploader::resizeRender('/images/not-found.png', '640', '425'),['class' => 'img-responsive','alt' => $property->sa_b]);?>
                        </picture>
                     </div>
                     <?php }?>
                  </div>
               </div>
            </div>
            <div class="col-md-4 about-grid" style="margin-top: 0em;  border:2px solid #F7882F; border-radius:5px;">
               <ul class="nav nav-tabs" style="width: 96%!important; margin-left: 1em!important;     border-bottom: #fff!important;">
                  <li class="active col-md-6 nav-grid">
                     <a data-toggle="tab" href="#booknow">Book Now</a>
                  </li>
                  <li class="col-md-6 nav-grid">
                     <a data-toggle="tab" href="#schedule">Schedule a Visit</a>
                  </li>
               </ul>
               <div class="tab-content">
              <div id="booknow" class="tab-pane fade in active">
               <?php
                if(!Yii::$app->user->isGuest)
               {?>

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id' => 'booking']]);
                  $booking = new booking();

                  ?>
                  <div class="form-group" style="margin-left:3.3em; margin-right: 2em; ">

                    <div class="row view" style="margin-bottom: -1em!important;">
                      <?= $form->field($booking, 'name')->textInput(['value' => $currentUser->first_name,'class' => 'form-control','readonly' => true])->label(false);?>
                    </div>
                    <div class="row view" style="margin-bottom: -1em!important;">
                  <?= $form->field($booking, 'email')->textInput(['value' => $contactDetails->email,'class' => 'form-control','readonly' => true])->label(false);?>
                    </div>
                    <div class="row view" style="margin-bottom: -1em!important;">
                     <?= $form->field($booking, 'mobile')->textInput(['value' => $contactDetails->mobile,'class' => 'form-control','maxlength' => 10,'readonly' => true])->label(false);?>
                    </div>
                    <div class="row view" style="margin-bottom: -1em!important;">
                    <?= $form->field($booking, 'occupancy')->dropDownList([
                      'singleroom' => 'Single Share',
                      'doubleshare' => 'Double Share',
                      'tripleshare' => 'Triple Share'],
                       ['prompt' => 'Select Occupancy'])->label(false);
                     ?>
                    <?= $form->field($booking,'property_id')->hiddenInput(['value' => $property->id])->label(false);?>
                    </div>
                    
                    <div class="row bookprice" style="margin-bottom: 0em!important; margin-left: -1em">
                      <div class="col-md-7">
                        <h4>TokenAmount</h4>
                      </div>
                      <div class="col-md-5" style="color:#f7882f" id="price">
                        <span id="pricehigh"></span>
                        <span id="pricelow"></span>
                        <h4 style="color: #F7882F!important">₹ <?= $tokenAmt;?></h4>
                      </div>
                    </div>
                        <div class="row" style="margin-bottom: 2em!important;">
                          <input type="submit" id="form-submit" class="btn btn-lg btn-success col-md-12 col-sm-12 col-xs-12" name="submit" value="Pay Now" style=" width: 103%!important; margin-left: 0em!important;margin-bottom: 0.2em!important;">
                          </div>
                        </div>
                  <?php ActiveForm::end(); ?>
                     <?php }else{?>
                     <div class="loginbook text-center">
                       <h3 style="line-height: 2em;">Please login to <br/>Book the Property</h3>
                
                      <a href="/site/signin" data-toggle="modal" id="signin-modal" style=" width: 96.5%;margin-top: 2em; background-color: #F7882F; margin-left: 0.3em; border:none;" class="btn btn-lg btn-success col-md-12 col-sm-12 col-xs-12 text-center">Login</a>
                    </div>
                     <?php }?>
                  </div>
                  <div id="schedule" class="tab-pane fade in ac">
                   <div class='col-sm-12 col-md-12' style="width: 94%!important;margin-left: 1.6em!important; margin-top: 0px;">

                  <?php if(!Yii::$app->user->isGuest){?>
                   <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id' => 'schedule_visits']]); 
                      $scheduledVisits = new scheduledVisits();
                   ?>
                  <div class="fade in active">
                     <div class="form-group" style="margin:1em;">
                    <div class="row view" style="margin-bottom: 5px!important; margin-top: -1em!important;" >
                     <?= $form->field($scheduledVisits, 'name')->textInput(['value' => $currentUser->first_name,'class' => 'form-control','readonly' => true])->label(false);?>
                    </div>
                    <div class="row view" style="margin-bottom: 5px!important;">
                    <?= $form->field($scheduledVisits, 'email')->textInput(['value' => $contactDetails->email,'class' => 'form-control','readonly' => true])->label(false);?>
                    </div>
                    <div class="row view" style="margin-bottom: 5px!important;">
                      <?= $form->field($scheduledVisits, 'mobile')->textInput(['value' => $contactDetails->mobile,'class' => 'form-control','maxlength' => 10,'readonly' => true])->label(false);?>
                    </div>
                    <div class="row" style="margin-bottom: 5px!important;">
                    <?= $form->field($scheduledVisits, 'occupancy')->dropDownList([
                      'singleroom' => 'Single Room',
                      'doubleshare' => 'Double Share',
                      'tripleshare' => 'Triple Share'],
                       ['prompt' => 'Select Occupancy'])->label(false);
                     ?>
                     <?= $form->field($scheduledVisits,'property_id')->hiddenInput(['value' => $property->id])->label(false);?>
                    </div>
                    </div>
                    <div class='input-group date' id='datetimepicker1' style="margin-bottom: 5px!important; margin-top: -1em;">
                      <?= $form->field($scheduledVisits,'scheduled_time')->textInput(['class' => "form-control input-group-addon col-md-12"])->label('Click to select date and Time');?>
                    </div>
                    <div class="row">
                        <input type="submit" id="form-submit" class="btn btn-lg btn-success col-md-12 col-sm-12 col-xs-12" name="submit" value="Submit" style="width: 92%!important;margin-left: 0.7em!important; margin-top: 0.8em!important; margin-bottom: 1.1em!important">
                    </div>
                  </div>
                  <?php ActiveForm::end(); ?>
                  <?php }else{?>
                  <div class="loginbook text-center">
                  <h3 style="line-height: 2em; margin-top: -1.7em">Please login to <br/>Schedule A Visit</h3>
                    <a href="/site/signin" data-toggle="modal" id="signin-modal" style=" width: 114.5%;margin-top: 2em; background-color: #F7882F; margin-left: -2em; border:none;" class="btn btn-lg btn-success col-md-12 col-sm-12 col-xs-12 text-center">Login</a>
                    </div>
                  <?php }?>
                  </div>
                  
                  </div>
            </div>
            <div class="clearfix"></div>
         </div>
         </div>
         <h3 class="desc text-center" style="color: #F7882F; margin-top: 2em!important;">Description</h3>
         <br />
            <p style="text-align:justify">
               <?=$property->description;?>
            </p>
      </div>
   </div>
   <div class="count-agileits" style="margin-top: -2em;">
      <div class="container">
         <h3 class="tittle" style="margin-bottom: 0.5em; color: #fff;">Amenities</h3>
         <div class="count-grids text-center">
         <div class="row col-md-12">
         <?php 
               $amenityModel = new Amenity();
               $propertyAmenity = new PropertyAmenities();
               $allAmenity = $amenityModel::find()->where(['del_status' => '0','status' => 'active'])->all();
               foreach($allAmenity as $loopCount => $amenity){
                $exists = $propertyAmenity::find()->where(['del_status' => '0','property_id' => $property->id,'amenity_id' => $amenity->id])->count();
            ?>
            <?=((($loopCount%12)==0) && ($loopCount!=0) && (!empty($allAmenity[$loopCount+1]))) ? '</div>
            <div class="clearfix"></div><div class="row col-md-12">' : ''?>
               <div style="<?=($exists == 0) ? 'opacity:0.5' : '';?>" class="col-md-1 count-grid">
                  <i class="<?= $amenity->icon_url;?>" aria-hidden="true"></i>
                  <div class="hvr-bounce-to-bottom">
                     <p style="color: #fff!important;"><?= $amenity->amenity_name;?></p>
                  </div>
               </div>
               <?php }?>
              </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
   </div>
   <!-- <div class="col-md-12" style="margin-top: 2em;">
      <h3 class="tittle2">Book Now</h3>
      <div class="col-md-2"></div>
      <div class="col-md-8 about-grid" style="border:2px solid #F7882F; padding:2em;border-radius:5px;">
         <ul class="nav nav-tabs" style="border-bottom: #fff!important;">
            <li class="active col-md-6">
               <a data-toggle="tab" aria-expanded="true" href="#roomwise">Book By Room</a>
            </li>
            <li class="col-md-6">
               <a data-toggle="tab" href="#bedwise">Book By Bed</a>
            </li>
         </ul>
         <div class="tab-content">
            <div id="roomwise" class="tab-pane active fade in">
              <div class="panel-default">
              <div class="panel-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="row" style="margin-top: -2em;">
                    <div class="col-md-6">
                      <div class="well">
                        <h4 class="text-danger"><span class="label label-success pull-right"><?=($property->rooms) ? $property->rooms[0]->price : 'No Rooms Available'?></span> Rent Amount </h4>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="well">
                        <h4 class="text-success"><span class="label label-warning pull-right"><?=($property->rooms) ? $property->rooms[0]->price : 'No Rooms Available'?></span> Security Amount </h4>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            </div>
            </div>
              <div class="col-md-12" style="margin-bottom: 2em;">
                <div class="row">
                    <div class="text-center">
                      <?php
                      $allRooms = $property->rooms;
                      $roomCount = count($allRooms);
                      if(is_array($property->rooms))
                      {
                        $count = 0;
                      foreach ($property->rooms as $roomKey => $room)
                      {
                        $roomCount = intval(count($property->rooms));
                        $unAvailableBeds = count($room->unavailableBed);
                        $colSize = \app\helpers\HtmlHelper::getColSize($roomCount);
                        $colClass = ($roomCount > 7) ? "col-md-2" : "col-md-".$colSize;
                        ?>
                      <div class=" <?= $colClass;?> <?= (intval($unAvailableBeds) > 0) ? 'inactive' : '';?> col-sm-4 text-center">
                      <div class="row">
                      <?=$room->room_name;?>
                      </div>
                     <div class="tool">
                      <img src="/images/room.png" height="80" width="80" style="font-size:3em; margin-top: 0.3em; margin-bottom: 0.2em;"><br />
                      <span class="tooltext"><?= (intval($unAvailableBeds) > 0) ? '<span class="tooltext back" style="margin-bottom:0.2em!important;">Occupied</span>' : 'Available';?></span>
                      </div>
                      <div class="row">
                      ₹<?= $room->price;?>
                      </div>
                      </div>
                      <?php 
                        $count++;
                        if($count==6)
                        {
                          break;
                        }
                      }}?>
                    </div>
                    </div>
                    </div>
                <?php if($roomCount > 3){?>
                <a href="/property/all-rooms?propId=<?=$property->id;?>" data-toggle="modal" data-target="#myModal" style=" width: 96.5%;margin-left: 1em; " class="btn btn-lg btn-success col-md-12 col-sm-12 col-xs-12 text-center" name="paynow">View All Rooms</a>
                  <?php } ?>
                  </div>
            <div id="bedwise" class="tab-pane fade in">
              <div class="panel-default" >
              <div class="panel-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="row" style="margin-top: -2em;">
                    <div class="col-md-6">
                      <div class="well">
                        <h4 class="text-danger"><span class="label label-success pull-right"><?=($property->rooms[0]->beds) ? $property->rooms[0]->beds[0]->price  : 'No Beds Available'?></span> Rent Amount </h4>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="well">
                        <h4 class="text-success"><span class="label label-warning pull-right"><?=($property->rooms[0]->beds) ? $property->rooms[0]->beds[0]->price : 'No Beds Available'?></span> Security Amount </h4>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            </div>
            </div>
                <div class="row">
                <div class="col-md-12" style="margin-bottom: 2em!important;margin-left:1em;">
                    <?php
                      $allBeds = $room->beds;
                      $bedCount = count($allBeds);
                      if(is_array($property->rooms))
                      {
                        $count = 0;
                      foreach ($property->rooms as $roomKey => $room)
                      {?>
                       <div class="col-md-4 room text-center">
                        <div class="row col-md-12">
                          <b style="margin-left: 2.3em"><?=$room->room_name;?></b>
                        </div>
                        <?php
                        if(is_array($room->beds))
                          $bedsCount = count($room->beds);
                          $colSize = \app\helpers\HtmlHelper::getColSize($bedsCount);
                        {?>
                      <div class="row">
                        <?php foreach ($room->beds as $bedKey => $bed)
                        {?>
                        <div class="tool bed <?= ($bed->available) ? '' : 'inactive';?> col-md-<?=$colSize;?>">
                        <div class="row">
                          <?= 'Bed '. $bed->bed_number;?>    
                        </div>
                          <span class="makeubig" data-tooltip-content="#showavailable">
                            <i class="fa fa-bed" aria-hidden="true" style="font-size:2.5em;margin-top: 0.3em; margin-bottom: 0.2em;"></i>
                          </span>    
                          <?= ($bed->available) ? '<span class="tooltext">Available</span>' : '<span class="tooltext back">Occupied</span>';?>                        
                        <div class="row">
                          ₹<?= $bed->price;?>
                        </div>
                      </div>
                      <?php }?>
                      </div>
                     <?php $count++;
                        if($count==3)
                        {
                          break;
                        }} ?> 
                      </div> 
                    <?php 
                    }}?> 
                    </div>
                     <?php if($roomCount > 3){
                      ?>
                    <a href="/property/all-beds?propId=<?=$property->id;?>" data-toggle="modal" data-target="#propertyModal" style=" margin-top: 1.5em!important; margin-bottom: -1.5em; width: 96.5%;" class="btn btn-lg btn-success col-md-12 col-sm-12 col-xs-12 text-center" name="paynow">View All Beds</a>
                   <?php } ?>
               </div>
               </div>
         </div>
         </div>
      </div>
      </div> -->
      <div class="col-md-2"></div>
   </div>
   <div class="clearfix"></div>
   <div class="contact-mub">
      <h2 class="tittle">Location Map</h2>
      <div class="map" id="realtime_canvas">
        
      </div>
      <input type="hidden" id="map-lat" value="<?=$property->lat;?>">
      <input type="hidden" id="map-lon" value="<?=$property->long;?>">
   </div>
   <?php if(!empty($similarProperty)){?>
   <div class="testimonials-mub" style=" margin-top: -3.9em;">
    <div class="container">
      <h3 class="title">Similar Properties</h3>
      <div class="row testimonial-grids mrgbot">
      <div class="col-md-10 col-md-offset-1">
      <div class="row">
         <?php foreach($similarProperty as $sprop){
            $propertyImage = ($sprop->propertyImages) ? $sprop->propertyImages[0]->url : '/images/notfound.jpg'  ;
          ?>
          <a href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($property->property_name);?>" class="vik" target="_blank">
              <div class="col-md-4 test-grid">
              <div class="panel panel-default">
                      <div class="panel-body test-grid" style="padding: 7px!important;">
                      
                        <?php echo Html::img(ImageUploader::resizeRender($propertyImage, '350', '243'),['class' => 'img-responsive','alt' => $property->sa_b]);?>
                      </div>
                      <div class="panel-footer" style=" background-color: #fff;">
                        <h4 class="card-title"><span class="pull-left"><?= ucwords($sprop->property_name);?></span><span class="pull-right">₹ <?=$sprop->show_price;?></span></h4><br /> 
                        <span title="<?=$sprop->sa_b;?>">
                     <p style="color: #6B7C8E!important; "><b><?=str_replace(', Haryana, India','',substr($sprop->sa_b ,0 , 26));?>...</b></p>
                     </span>
                      </div>
                    </div>
              <div class="clearfix"></div>
              </div>
            </a>
          <?php }?>
        <div class="clearfix"></div>
      </div>
      </div>
   </div>
</div>
</div>
<?php }?>
        <?php 
        if(!Yii::$app->user->isGuest){?>
<div class="modal fade" id="confirmPrice" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-info" id="price-with-tax">
             <div class="modal-header">
    <div class="row container-fluid">
        <div class="pull-right"><button style="display:inline" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    </div>                        
</div>
<div class="modal-body real-spa">
<div class="login-grids">
    <div class="login">
        <div class="login-right">
        <?php
          // Merchant key here as provided by Payu
          //payu test key : rjQUPktU , Salt: e5iIg1jwi8
          $MERCHANT_KEY = "GbnogOPG";

          // Merchant Salt as provided by Payu
          $SALT = "RK4rlkOAr8";

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";

          $formError = 0;
          $action = $PAYU_BASE_URL . '/_payment';

        $form = ActiveForm::begin(['layout' => 'horizontal','options' => ['id' => 'confirm-payment','name' => 'payuForm','method' => 'POST','data-pjax' => true],'action' => $action]); ?>
               <div class="col-md-12 text-center" style="margin-bottom: 1em; margin-top: -2em">
                  <h2 style="font-size: 26px">Your Amount and Tax Details</h2>
               </div>
                  <input type="hidden" id="key" name="key" value="<?=$MERCHANT_KEY;?>">
                  <input type="hidden" id="txnid" name="txnid" value="<?=$currentUser->username.'_'.time();?>">
               <?php $taxAmount = TaxHelper::calculateTax('3.5',$tokenAmt);?>

               <center><h4 style="color:#6B7C8E">Following are Your payment details:</h4></center>

                <?= $form->field($booking, 'price')->textInput(['name' => 'actual_price','class' => 'form-control','value' => $tokenAmt])->label('Token Amount');?>

                 <?= $form->field($booking, 'tax')->textInput(['class' => 'form-control','value' => $taxAmount,'disabled' => true])->label('Tax Amount');?>

                 <hr style="color:#6B7C8E;margin-top:0px" />
                 <?= $form->field($booking, 'total')->textInput(['name' => 'amount','class' => 'form-control','value' => ($tokenAmt+$taxAmount),'disabled' => true])->label('Total Amount');?>
                  <input type="hidden" id="productinfo" name="productinfo" value="booking_room">
                  <input type="hidden" id="propertId" name="propertyId" value="<?=$property->id;?>">
                  <input type="hidden" id="firstname" name="firstname" value="<?=$currentUser->first_name;?>">
                  <input type="hidden" id="lastname" name="lastname" value="<?=$currentUser->last_name;?>">
                  <input type="hidden" id="email" name="email" value="<?=$contactDetails->email;?>">
                  <input type="hidden" id="address1" name="address1" value="<?=$property->sa_a;?>">
                  <input type="hidden" id="address2" name="address1" value="<?=$property->sa_b;?>">
                  <input type="hidden" id="city" name="city" value="<?=$property->city_name;?>">
                  <input type="hidden" id="state" name="state" value="<?=$property->state->state_name;?>">
                  <input type="hidden" id="phone" name="phone" value="<?=$contactDetails->mobile;?>">
                  <input type="hidden" id="surl" name="surl" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/payu/success.php';?>">
                  <input type="hidden" id="furl" name="furl" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/payu/failure.php';?>">
                  <input type="hidden" id="udf1" name="udf1" value="">
                  <input type="hidden" id="udf2" name="udf2" value="">
                  <input type="hidden" id="udf3" name="udf3" value="">
                  <input type="hidden" id="udf4" name="udf4" value="">
                  <input type="hidden" id="udf5" name="udf5" value="">
                  <input type="hidden" id="udf6" name="udf6" value="">
                  <input type="hidden" id="udf7" name="udf7" value="">
                  <input type="hidden" id="udf8" name="udf8" value="">
                  <input type="hidden" id="udf9" name="udf9" value="">
                  <input type="hidden" id="udf10" name="udf10" value="">
                  <input type="hidden" id="service_provider" name="service_provider" value="payu_paisa">
                 <br /><br />
                <input type="submit" value="Continue to Payment" >
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>
  <div class="modal-footer">  
</div>
</div>
</div>
</div>
<?php }?>
<?php $this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js", [
    'depends' => [\yii\web\JqueryAsset::className()]]);?>

<?php $this->registerJsFile("/js/bootstrap-datetimepicker.min.js", [
    'depends' => [\yii\web\JqueryAsset::className()]]);?>
