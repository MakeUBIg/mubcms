<?php 
   use yii\helpers\Html;
   use \app\helpers\ImageUploader;
   use yii\widgets\LinkPager;
   ?>        
      <input type="hidden" name="result_count" id="result_count" value="<?=$result['result_count']?>">
     <?php if($result['result_count'] > 0){?>
      <div class="agent-mub">
    <?php foreach($result['result'] as $res){ 
      $propertyImage = ($res->propertyImages) ? $res->propertyImages[0]->url : '/images/notfound.jpg' ;
      ?>
    <div style="padding-left:0px" class="col-md-12 agent-grid">
    <div class="agent-left multi-gd-text">
      <a target="_blank" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($res->property_name);?>" class='vik'>
      <?php echo Html::img(ImageUploader::resizeRender($propertyImage, '350', '252'),['class' => 'img-responsive','alt' => $res->sa_b]);?></a>
      </div>
      <div class="agent-right listviewbg">
        <h3 class="prop-desc" style="margin-top: -0.4em!important"><?= ucwords($res['property_name']);?></h3>
        <ul>
          <li class="pull-right hidden-xs price"><h3 class="prop-desc">Price : ₹ <?=$res->show_price;?> onwards</h3></li>
          <li class="hidden-md hidden-sm hidden-lg price2"><h4 class="prop-desc">Price : ₹ <?=$res->show_price;?> onwards</h4></li>
          <a target="_blank" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($res->property_name);?>" style="text-decoration: none!important;" ><li class="addr"><b><?=str_replace(', India','',$res->sa_b);?></b></li></a>
          <li class="agent-grid p descr"><p><?=substr(str_replace('&nbsp;', ' ', strip_tags($res->description)) ,0 , 150);?>...</p></li>
        </ul>
        <div class="pull-right" style="padding-bottom: 0.4em;margin-top: -0.8em!important;">
                           <a href="https://www.facebook.com/sharer.php?u=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-primary" style=" padding-left: 0.8em; padding-right: 0.8em; padding-top: 0.3em;"><b>f</button></a>

                           <a href="https://plus.google.com/share?url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-danger" style="padding-left: 0.5em; padding-right: 0.5em; padding-top: 0.3em;"><b>G+</button></a>

                           <a href="https://twitter.com/share?text=OSMSTAYS Property;?>!&amp;url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-info" style="padding-left: 0.8em; padding-right: 0.8em; padding-top: 0.3em;" target="_blank"><b>t</button></a>
                     </div>

      </div>
  </div>
      <?php }}else{?>
   <h2>Sorry There was no result matching your request!</h2>
   <?php }?>
    <div class="clearfix"></div>
    <?php if($result['result_count'] > 0){?>
   <center><?= LinkPager::widget([
    'pagination' => $result['pages'],
    ]);
   ?></center><?php }?>
   </div>