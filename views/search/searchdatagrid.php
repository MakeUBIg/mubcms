<?php 
   use yii\helpers\Html;
   use \app\helpers\ImageUploader;
   use yii\widgets\LinkPager;
   if(!isset($result['rooms_type']))
   {
     $result['rooms_type'] = 'hostel'; 
   }
   if(!isset($result['rooms_for']))
   {
      $result['rooms_for'] = 'boys';
   }
   ?>
<div class="company-grids">
<input type="hidden" name="result_count" id="result_count" value="<?=$result['result_count']?>">
<?php if($result['result_count'] > 0){?>
   <?php foreach($result['result'] as $res){ 
      $propertyImage = ($res->propertyImages) ? $res->propertyImages[0]->url : '/images/notfound.jpg';
      ?>
   <div class="col-md-4 company-grid multi-gd-text">
   <a target="_blank" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($res->property_name);?>" class="vik"><?php echo Html::img(ImageUploader::resizeRender($propertyImage, '255', '143'),['class' => 'img-responsive','alt' => $res->sa_b]);?></a>

    <div class="wid" style="background-color: #aab2bf!important; margin-top: -0.7em!important;"><br/>
     <h4>
     <span class="pull-left" style="color: #EEEEEE!important;    padding-left: 0.7em"><?= ucwords($res['property_name']);?>

     </span></h4><br/>
     <span title="<?=$res->sa_b;?>">
                     <a target="_blank" class="heigh" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($res->property_name);?>" style="text-decoration: none!important;"><p style="color: #EEEEEE!important; padding-left: 0.9em;padding-right: 0.7em;"><b><?=str_replace(', Haryana, India','',substr($res->sa_b ,0 , 26));?>...</b></p></a>
                     </span>

     <br />
     <span class="pull-left" style="color: #FFF!important;  font-weight: bold;padding-left: 1.1em;margin-top: -1em;">₹ <?=$res->show_price;?> onwards</span><br/>
                     <h6 class="address p"><p><?=substr(str_replace('&nbsp;', ' ', strip_tags($res->description)) ,0 , 80);?>...</p></h6>
                     <div class="text-center" style="padding-bottom: 0.4em;">
                           <a href="https://www.facebook.com/sharer.php?u=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-primary" style=" padding-left: 0.8em; padding-right: 0.8em; padding-top: 0.3em;"><b>f</button></a>

                           <a href="https://plus.google.com/share?url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-danger" style="padding-left: 0.5em; padding-right: 0.5em; padding-top: 0.3em;"><b>G+</button></a>

                           <a href="https://twitter.com/share?text=OSMSTAYS Property;?>!&amp;url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-info" style="padding-left: 0.8em; padding-right: 0.8em; padding-top: 0.3em;" target="_blank"><b>t</button></a>
                     </div>

     </div>
   </div>
   <?php }}else{?>
   <h2>Sorry There was no result matching your request!</h2>
   <?php }?>
   <div class="clearfix"></div>
   <?php if($result['result_count'] > 0){?>
   <center><?= LinkPager::widget([
    'pagination' => $result['pages'],
    ]);
   ?></center><?php }?>
</div>