<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<h1 class="page-title">Manangement Team</h1>
							<p>Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat do eiusmod tempor incidid.</p>						
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="index.html">Home</a></li>
								<li><a href="about-us.html">About Us</a></li>
								<li class="active"><span>Our Team</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="/images/192012.jpg" alt="" />
			</div>
		</div>
		<!-- #end Banner/Static -->
	</header>
	<!-- End Header -->
	<!-- Content Section -->
	<!-- <div class="section section-contents bg-light section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="wide-md center">
					<h2>Board of Directors</h2>
					<p>Lorem ipsum dolor sit amet consectetur to adipiscing elit sed dot eiusmod tempor incididunt labore et dolore magna aliqua. Veniam quis nostrud exercitation ullamco laboris .</p>
				</div>
				<div class="gaps size-2x"></div>
				<div class="team-member-row row">
					<div class="col-md-6 res-m-bttm-lg">
						<div class="team-profile">
						<div class="team-member row">
							<div class="team-photo col-md-4 col-sm-4 col-xs-12">
								<img alt="" src="/images/team-a.jpg">
							</div>
							<div class="team-info col-md-8 col-sm-8 col-xs-12">
								<h3 class="name">Andrew Cole</h3>
								<p class="sub-title">- Managing Director and CEO</p>
								<p>Lorem ipsum dolor sit amet consectetur to adipiscing elit, sed dot eiusmod tempor incididunt labore et dolore magna aliqua. Veniam quis nostrud exercitation ullamco laboris nisiut. Lorem ipsum dolor sit amet consectetur to adipiscing elit, sed dot eiusmod tempor incididunt. Veniam quis nostrud exercitation ullamco labor.</p>
							</div>
						</div>
					</div>
					</div>
					<div class="col-md-6">
						<div class="team-profile">
							<div class="team-member row">
								<div class="team-photo col-md-4 col-sm-4 col-xs-12">
									<img alt="" src="/images/team-b.jpg">
								</div>
								<div class="team-info col-md-8 col-sm-8 col-xs-12">
									<h3 class="name">Nicholas Mark</h3>
									<p class="sub-title">- Chief Financial Officer</p>
									<p>Lorem ipsum dolor sit amet consectetur to adipiscing elit, sed dot eiusmod tempor incididunt labore et dolore magna aliqua. Veniam quis nostrud exercitation ullamco laboris nisiut. Lorem ipsum dolor sit amet consectetur to adipiscing elit, sed dot eiusmod tempor incididunt labore dolore magna aliqua nisiut.</p>

								</div>
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
		</div>		
	</div> -->
	<!-- End Section -->
	<!-- Content Section -->
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="wide-md center">
					<h2>Executive Team</h2>
					<p>Lorem ipsum dolor sit amet consectetur to adipiscing elit sed dot eiusmod tempor incididunt labore et dolore magna aliqua. Veniam quis nostrud exercitation ullamco laboris .</p>
				</div>
				<div class="team-member-row row">
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Robert Miller</h4>
								<p class="sub-title">Chief Accountant</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Stephen Everett</h4>
								<p class="sub-title">Chief Operating Officer</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Philip Hennessy</h4>
								<p class="sub-title">Non-Executive Director</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Nicholas Miller</h4>
								<p class="sub-title">Head of Human Resources</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
				</div><!-- TeamRow #end -->
				<div class="team-member-row row">
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Robert Miller</h4>
								<p class="sub-title">Chief Accountant</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Stephen Everett</h4>
								<p class="sub-title">Chief Operating Officer</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Philip Hennessy</h4>
								<p class="sub-title">Non-Executive Director</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Nicholas Miller</h4>
								<p class="sub-title">Head of Human Resources</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
				</div>
				
			</div>
		</div>		
	</div>
	<!-- End Section -->	
	<!-- Client logo -->
	