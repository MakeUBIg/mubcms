<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<h1 class="page-title">What Client's Says</h1>
							<p>To ventore veritatis et quasi architecto beatae vitae dicta.</p>						
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="index.html">Home</a></li>
								<li><a href="about-us.html">About</a></li>
								<li class="active"><span>Testimonial</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="/images/banner-inside-a.jpg" alt="" />
			</div>
		</div>
		<!-- #end Banner/Static -->
	</header>
	<!-- End Header -->

	<!-- Content -->
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="row">

				<div class="wide-md">
					<div class="testimonials testimonials-list">
						<div class="quotes">
							<div class="quotes-text">
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
							</div>
							<div class="profile">
								<h5>John Doe</h5>
								<h6>CEO, Company Name</h6>
							</div>
						</div>
						<div class="quotes">
							<div class="quotes-text">
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
							</div>
							<div class="profile">
								<h5>Michel Walton</h5>
								<h6>CEO, Company Name</h6>
							</div>
						</div>
						<div class="quotes">
							<div class="quotes-text">
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
							</div>
							<div class="profile">
								<h5>Rojer Murr</h5>
								<h6>CEO, Company Name</h6>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- End Content -->	
	<!-- Client logo -->
	