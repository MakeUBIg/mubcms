<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<h1 class="page-title">Our Services</h1>
							<p>Core3 is one of the fastest growing International VoIP carrier in Hongkong.Core3 offers a complete portfolio of connections to Europe, North and South-America, Africa, Asia and the Middle-East</p>						
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="index.html">Home</a></li>
								<li class="active"><span>Our Services</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="/images/192012.jpg" alt="" />
			</div>
		</div>
		<!-- #end Banner/Static -->
	</header>
	<!-- End Header -->
	<!-- Service Section -->
	<div class="section section-services section-pad">
	    <div class="container">
	        <div class="content row">
	        	
	        	<div class="wide-md center">
	        		<h2 class="heading-lead"><b>Core3 Networks Services</b></h2>
					<p class="cente">Are you an international wholesale carrier interested to interconnect with Core3  Network?</p>
					<p>Core3 is one of the fastest growing International VoIP carrier in Hongkong.Core3 offers a complete portfolio of connections to Europe, North and South-America, Africa, Asia and the Middle-East. Core3 routes millions of international voice minutes from other carriers through state of the art switching platform. 
Core3 is known as the most aggressive, flexible and quality-driven company in the wholesale arena; we are always searching for VoIP and SS7-carriers offering good quality routes at the best tariffs available. Core3 maintains long term relationships with PTT's, incumbents and mobile operators from all over the world. We have interconnected with various global network providers through our points of presence and we are continually connecting more and more voice carriers to meet the demand of our partners.</p>
	        	</div>
	        	<!-- Feature Row  -->
				<div class="feature-row feature-service-row row">
					<div class="col-md-4 col-sm-6 even first">
						<!-- feature box -->
						<div class="feature boxed">
							<a href="#">
								<div class="fbox-photo">
									<img src="/images/770.jpg" alt="">
								</div>
							</a>
							<div class="fbox-content">
								<h3 class="lead"><a href="#">Voice Termination Services</a></h3>
								<p>Core3 offers Domestic & International termination services and direct routes for Telecom operators, resellers and companies.</p>
								<p><a href="#" class="btn-link link-arrow-sm">Learn More</a></p>
							</div>
						</div>
						<!-- End Feature box -->
					</div>
					<div class="col-md-4 col-sm-6 odd">
						<!-- feature box -->
						<div class="feature boxed">
							<a href="#">
								<div class="fbox-photo">
									<img src="/images/770.jpg" alt="">
								</div>
							</a>
							<div class="fbox-content">
								<h3 class="lead"><a href="#">VOIP Wholesale Services</a></h3>
								<p>Core3 offers Domestic & International termination services and direct routes for Telecom operators, resellers and companies.</p>
								<p><a href="#" class="btn-link link-arrow-sm">Learn More</a></p>
							</div>
						</div>
						<!-- End Feature box -->
					</div>
					<div class="col-md-4 col-sm-6 even">
						<!-- feature box -->
						<div class="feature boxed">
							<a href="#">
								<div class="fbox-photo">
									<img src="/images/770.jpg" alt="">
								</div>
							</a>
							<div class="fbox-content">
								<h3 class="lead"><a href="#">Voice Termination Services</a></h3>
								<p>Core3 offers Domestic & International termination services and direct routes for Telecom operators, resellers and companies.</p>
								<p><a href="#" class="btn-link link-arrow-sm">Learn More</a></p>
							</div>
						</div>
						<!-- End Featur box -->
					</div>
					<div class="col-md-4 col-sm-6 odd first">
						<!-- featured box -->
						<div class="feature boxed">
							<a href="#">
								<div class="fbox-photo">
									<img src="/images/770.jpg" alt="">
								</div>
							</a>
							<div class="fbox-content">
								<h3 class="lead"><a href="#">Insurance Consulting</a></h3>
								<p>Lorem ipsum dolor sit amet, conse ctetur adip isicing elit, sed do eius mod tempor incididunt fuga platea ut labore et.</p>
								<p><a href="#" class="btn-link link-arrow-sm">Learn More</a></p>
							</div>
						</div>
						<!-- End Feature box -->
					</div>
					<div class="col-md-4 col-sm-6 odd">
						<!-- featured box -->
						<div class="feature boxed">
							<a href="#">
								<div class="fbox-photo">
									<img src="/images/770.jpg" alt="">
								</div>
							</a>
							<div class="fbox-content">
								<h3 class="lead"><a href="#">Taxes Consulting</a></h3>
								<p>Lorem ipsum dolor sit amet, conse ctetur adip isicing elit, sed do eius mod tempor incididunt fuga platea ut labore et.</p>
								<p><a href="#" class="btn-link link-arrow-sm">Learn More</a></p>
							</div>
						</div>
						<!-- End Feature box -->
					</div>
					<div class="col-md-4 col-sm-6 odd">
						<!-- featured box -->
						<div class="feature boxed">
							<a href="#">
								<div class="fbox-photo">
									<img src="/images/770.jpg" alt="">
								</div>
							</a>
							<div class="fbox-content">
								<h3 class="lead"><a href="#">Business Audit</a></h3>
								<p>Lorem ipsum dolor sit amet, conse ctetur adip isicing elit, sed do eius mod tempor incididunt fuga platea ut labore et.</p>
								<p><a href="#" class="btn-link link-arrow-sm">Learn More</a></p>
							</div>
						</div>
						<!-- End Feature box -->
					</div>
				</div>
				<!-- Feture Row  #end -->

	        </div>
	    </div>
	</div>
	<!-- End Section -->	
	<!-- Client logo -->
	