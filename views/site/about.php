
<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<h1 class="page-title">About Core3 Networks</h1>
							<p>Core3 offers Domestic & International termination services and direct routes for Telecom operators, resellers and companies.  </p>						
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="index.html">Home</a></li>
								<li class="active"><span>About Us</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="/images/192012.jpg" alt="" />
			</div>
		</div>
		<!-- #end Banner/Static -->
	</header>
	<!-- End Header -->
	<!-- Content -->
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row row-vm">

				<div class="col-sm-7 npl pad-r res-m-bttm">
					<h2 class="heading-lg">About Us</h2>
					<p class="lead"><strong>Core3 Network </strong>offers A-Z as well as direct routes for many countries worldwide. Our platform is backed by superior Billing System and competitive rates to assure your calls get connected with the highest quality of service in the market. 
We provide our clients a secured Web interface so they can view their usage, balance information and route statistics. Our system supports both H323 and SIP protocol and traffic is accepted from H323 and SIP Gateways, Soft switches, or any other 3rd party VoIP platform.</p>
				</div>
				<div class="col-sm-5 npr">
					<img class="img-shadow" alt="" src="/images/770.jpg">
				</div>

			</div>
		</div>
	</div>
	<!-- End content -->

	<!-- Why Choose -->
	<div class="section section-contents section-pad light has-bg fixed-bg" style="background-image: url(/images/plx-dark.jpg);">
		<div class="container">
			<div class="content row">

				<h2 class="heading-lg">Why Choose Us</h2>
				
				<div class="clear"></div>
				<div class="feature-intro space-top">

					<div class="row">
						<div class="col-sm-3 res-s-bttm">
							<div class="icon-box style-s1">
								<em class="fa fa-briefcase" aria-hidden="true"></em>
							</div>
							<h4>Experience</h4>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
						</div>
						<div class="col-sm-3 res-s-bttm">
							<div class="icon-box style-s1">
								<em class="fa fa-black-tie" aria-hidden="true"></em>
							</div>
							<h4>Professional</h4>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
						</div>
						<div class="col-sm-3 res-s-bttm">
							<div class="icon-box style-s1">
								<em class="fa fa-cogs" aria-hidden="true"></em>
							</div>
							<h4>Safety</h4>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
						</div>
						<div class="col-sm-3">
							<div class="icon-box style-s1">
								<em class="fa fa-building" aria-hidden="true"></em>
							</div>
							<h4>Sustainability</h4>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-3 res-s-bttm">
							<div class="icon-box style-s1">
								<em class="fa fa-diamond" aria-hidden="true"></em>
							</div>
							<h4>Integrity</h4>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
						</div>
						<div class="col-sm-3 res-s-bttm">
							<div class="icon-box style-s1">
								<em class="fa fa-check-square" aria-hidden="true"></em>
							</div>
							<h4>Reliable</h4>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
						</div>
						<div class="col-sm-3 res-s-bttm">
							<div class="icon-box style-s1">
								<em class="fa fa-bolt" aria-hidden="true"></em>
							</div>
							<h4>Great Managment</h4>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
						</div>
						<div class="col-sm-3">
							<div class="icon-box style-s1">
								<em class="fa fa-lightbulb-o" aria-hidden="true"></em>
							</div>
							<h4>Innovation</h4>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
						</div>
					</div>

				</div>

			</div>			
		</div>
	</div>
	<!-- End Why Choose -->

	<!-- Testimonials -->
	<div class="section section-quotes section-pad bg-light">
		<div class="container">
			<div class="content row">
			
				<div class="col-md-offset-2 col-md-8 center">
					<h2 class="heading-lg">Testimonials</h2>
				</div>
				<div class="gaps"></div>
				<div class="testimonials">
					<div id="testimonial" class="quotes-slider quotes-boxed col-md-8 col-md-offset-2">
						<div class="owl-carousel loop has-carousel" data-items="1" data-loop="true" data-dots="true" data-auto="true">
							<div class="item">
								<!-- Each Quotes -->
								<div class="quotes">
									<div class="quotes-text center">
										<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo nemo enim ipsam.</p>
									</div>
									<div class="profile">
										<img src="/images/160.jpg" alt="">
										<h5>Maria jaqline</h5>
										<h6>CEO, Company Name</h6>
									</div>
								</div>
								<!-- End Quotes -->
							</div>
							<!-- End Slide -->
							<!-- Each Slide -->
							<div class="item">
								<!-- Each Quotes -->
								<div class="quotes">
									<div class="quotes-text center">
										<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</p>
									</div>
									<div class="profile">
										<img src="/images/160.jpg" alt="">
										<h5>Maria jaqline</h5>
										<h6>CEO, Company Name</h6>
									</div>
								</div>
								<!-- End Quotes -->
							</div>
						</div>
						<!-- End Slide -->
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- End Section -->
	<!-- Teams -->
	<div class="section section-teams section-pad">
		<div class="container">
			<div class="content row">

				<div class="row">
					<div class="col-md-10 col-md-offset-1 center">
						<h2 class="heading-lg">Our Executive Team</h2>
						<p>Lorem ipsum dolor sit amet consectetur to adipiscing elit labore et dolore. Eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam. Duis aute irure dolor nostrud ullamco.</p>
					</div>
				</div>
				<div class="team-member-row row">
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Robert Miller</h4>
								<p class="sub-title">Managing Director &amp; CEO</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Stephen Everett</h4>
								<p class="sub-title">Chief Operating Officer</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Philip Hennessy</h4>
								<p class="sub-title">Non-Executive Director</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Robert Miller</h4>
								<p class="sub-title">Head of Human Resources</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
				</div><!-- TeamRow #end -->
			</div>
		</div>
	</div>			