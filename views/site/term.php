<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<h1 class="page-title">Term & Condition</h1>					
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="/">Home</a></li>
								<li class="active">Term & Condition</li>
								
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="/images/192012.jpg" alt="" />
			</div>
		</div>
		<!-- #end Banner/Static -->
	</header>
<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="row">
				<div class="col-md-1"></div>
					<div class="col-md-10">

						
												
						<h3 class="color-primary"><b>1. Term of Use</b></h3>
						<p class="just">Unlawful or Unacceptable Use of the Core3 Networks Pvt. Ltd. You may not use, or permit the use of, the Core3 for unlawful purposes or for purposes that Core3 finds unacceptable. In order to fulfill this obligation, you may not transmit, post or receive certain material, which include but are not limited to the following: threatening, abusive, libelous, defamatory, obscene, pornographic, profane, or otherwise objectionable information of any kind, including without limitation any transmissions constituting or encouraging conduct that would result in a criminal offense or civil liability, or otherwise violate any local, state, national or international laws or regulations. Moreover, you may not (i) Transmit any information or software which contains a virus, worm, Trojan Horse, or other harmful component; (ii) Transmit any information, software or other material that is protected by copyright or other proprietary right (including trade secret materials), or derivative works thereof, without obtaining permission of the copyright owner or right holder; (iii) Transmit any bulk email, whether or not solicited; or (iv) Transmit any unsolicited bulk email (also known as "spam"). You agree to indemnify Core3 and hold Core3 harmless from any and all claims, damages, losses, and expenses (including attorney's fees and expenses) resulting from or allegedly resulting from your use of the Core3 whether or not such use is found to be in violation of any statute, rule or regulation. IMPORTANT NOTE: IF YOU VIOLATE THIS ACCEPTABLE USE POLICY, YOUR ACCOUNT MAY BE SUSPENDED OR TERMINATED WITHOUT NOTICE, AND WITHOUT A REFUND, AT THE DISCRETION OF Core3. </p>
						<!-- Acorrdion Panels -->
						<div class="panel-group accordion" id="general" role="tablist" aria-multiselectable="true">
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i1">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i1" aria-expanded="false">
											<h3 class="color-primary"><b>2. Term of Use</b></h3>
											<span class="plus-minus"><span></span></span>
										</a>
									</h4>
								</div>
								<div id="ques-ans-i1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i1">
									<div class="panel-body">
										  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit edeo eiusmod. Tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam . Quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat donec id elit non porta gravida..</p>
									</div>
								</div>
							</div> 
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i2">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i2" aria-expanded="false">
											<h3 class="color-primary"><b>3. Term of Use</b></h3>
											<span class="plus-minus"><span></span></span>
										</a>
									</h4>
								</div>
								<div id="ques-ans-i2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i2">
									<div class="panel-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Donec id elit non mi porta gravida at eget metus. Quise nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat laboris nisie.</p>
									</div>
								</div>
							</div>
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   <h3 class="color-primary"><b>4. Term of Use</b></h3>
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat.</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   <h3 class="color-primary"><b>5. Term of Use</b></h3>
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat.</p>
									</div>
								</div>
							</div><div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   <h3 class="color-primary"><b>6. Term of Use</b></h3>
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat.</p>
									</div>
								</div>
							</div>
							<!-- each panel -->
						</div>
						<!-- End Acorrdion -->
						
						<h3 class="color-secondary">Areas of Expertise</h3>
						<p>Bring to the table dolor sit amet enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nostrud ullamco. Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<p>If you have any questions regarding our services, please <strong>contact us</strong> or call at <strong>800 1234 5677</strong>.</p>
						
					</div>
					
					<!-- Sidebar -->
					<!-- <div class="col-md-4">
						<div class="sidebar-right">

							<div class="wgs-box wgs-menus">
								<div class="wgs-content">
									<ul class="list list-grouped">
										<li class="list-heading">
											<span>Our Solutions and Services</span>
											<ul>
												<li><a href="service-single.html">Loan Sanction</a></li>
												<li class="active"><a href="service-single-alter.html">Investment Planning</a></li>
												<li><a href="service-single.html">Insurance Consulting</a></li>
												<li><a href="service-single-alter.html">Taxes Consulting</a></li>
												<li><a href="service-single.html">Business Audit</a></li>
												<li><a href="service-single-alter.html">Mutual Funds</a></li>
											</ul>
										</li>
									</ul>									
								</div>
							</div>
							
							<div class="wgs-box boxed light has-bg">
								<div class="wgs-content">
									<h3>Innovative Tools for Investor</h3>
									<p>We employ a long-established strategy, sector-focused investing across all of our markets globally...</p>
									<a href="service-single.html" class="btn btn-alt btn-outline"> Learn More</a>
								</div>
								<div class="wgs-bg imagebg">
									<img src="/images/photo-sd-a.jpg" alt="">
								</div>
							</div>
							
							<div class="wgs-box boxed bg-secondary light">
								<div class="wgs-content">
									<h3>Need Help To Grow Your Business?</h3>
									<p>Investment Expert will help you start your own company.</p>
									<a href="contact.html" class="btn btn-light"> Get In Touch</a>
								</div>
							</div>

						</div>
					</div> -->
					<!-- Sidebar #end -->
				</div>
				
			</div>
		</div>		
	</div>