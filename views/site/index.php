<div id="slider" class="banner banner-slider slider-large carousel slide carousel-fade">
			<!-- Wrapper for Slides -->
			<div class="carousel-inner">
				<div class="item active">
					<!-- Set the first background image using inline CSS below. -->
					<div class="fill" style="background-image:url('images/19802.jpg');">
						<div class="banner-content">
							<div class="container">
								<div class="row">
									<div class="banner-text al-left pos-left light">
										<h2>Core3 Networks Pvt. Ltd.</h2>
										<p class="p1">Core3 is one of the fastest growing International VoIP carrier in Hongkong. offering you a complete portfolio of connections to Europe.....</p>
										<a href="#" class="btn">Learn more</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<!-- Set the second background image using inline CSS below. -->
					<div class="fill" style="background-image:url('images/1980234.jpg');">
						<div class="banner-content">
							<div class="container">
								<div class="row">
									<div class="banner-text al-left pos-left light">
										<h2>Expert Financial Advice.</h2>
										<p class="p1">We help clients find ways to turn into actionable insights by embedding economics across their organization’s strategy.</p>
										<a href="#" class="btn">Learn more</a>
									</div>
								</div>
							</div>
						</div>					
					</div>
				</div>
				<div class="item">
					<!-- Set the second background image using inline CSS below. -->
					<div class="fill" style="background-image:url('images/1980-2.jpg');">
						<div class="banner-content">
							<div class="container">
								<div class="row">
									<div class="banner-text al-left pos-left light">
										<h2>Expert Financial Advice.</h2>
										<p class="p1">We help clients find ways to turn into actionable insights by embedding economics across their organization’s strategy.</p>
										<a href="#" class="btn">Learn more</a>
									</div>
								</div>
							</div>
						</div>					
					</div>
				</div>
			</div>
			<!-- Left and right controls -->
			<a class="left carousel-control" href="#slider" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#slider" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<!-- #end Banner/Slider -->
	</header>
	<!-- End Header -->
	
	<!-- Service Section -->
	<div class="section section-services">
	    <div class="container">
	        <div class="content row">
				<!-- Feature Row  -->
				<div class="feature-row feature-service-row row feature-s4 off-text boxed-filled boxed-w">
					<div class="heading-box clearfix">
						<div class="col-sm-3">
							<h2 class="heading-section">Core3 Specialists</h2>
						</div>
						<div class="col-sm-8 col-sm-offset-1">
							<span>Core3 is one of the fastest growing International VoIP carrier in Hongkong. offering you a complete portfolio of connections to Europe.</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Feature box -->
						<div class="feature">
							<a href="#">
								<div class="fbox-photo">
									<img src="images/770.jpg" alt="">
								</div>
								<div class="fbox-over">
									<h3 class="title">Market Analysis</h3>
									<div class="fbox-content">
										<p>For many residents throughout consec adipis elit sedo eius.</p>
										<span class="btn">Learn More</span>
									</div>
								</div>
							</a>
						</div>
						<!-- End Feature box -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Feature box -->
						<div class="feature">
							<a href="#">
								<div class="fbox-photo">
									<img src="images/770.jpg" alt="">
								</div>
								<div class="fbox-over">
									<h3 class="title">Accounting Advisor</h3>
									<div class="fbox-content">
										<p>Our finance experience amet consec adipis elitmod tempor.</p>
										<span class="btn">Learn More</span>
									</div>
								</div>
							</a>
						</div>
						<!-- End Feature box -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Feature box -->
						<div class="feature">
							<a href="#">
								<div class="fbox-photo">
									<img src="images/770.jpg" alt="">
								</div>
								<div class="fbox-over">
									<h3 class="title">General Consultancy</h3>
									<div class="fbox-content">
										<p>Sidid ipsum dolor sit amet consec adlit sed usmod cons.</p>
										<span class="btn">Learn More</span>
									</div>
								</div>
							</a>
						</div>
						<!-- End Feature box -->
					</div>
				
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Feature box -->
						<div class="feature">
							<a href="#">
								<div class="fbox-photo">
									<img src="images/770.jpg" alt="">
								</div>
								<div class="fbox-over">
									<h3 class="title">Structured Assessment</h3>
									<div class="fbox-content">
										<p>Sidid ipsum dolor sit amet consec adipis elit sed do eiusmo dcons.</p>
										<span class="btn">Learn More</span>
									</div>
								</div>
							</a>
						</div>
						<!-- End Feature box -->
					</div>					
				</div>
				<!-- Feture Row  #end -->

	        </div>
	    </div>
	</div>
	<!-- End Section -->

	<!-- Content -->
	<div class="section section-content section-pad">
		<div class="container">
			<div class="content row">

				<div class="row row-vm">
					<div class="col-md-6 res-m-bttm">
						<h5 class="heading-sm-lead">About us</h5>
						<h2 class="heading-section">Who we are</h2>
						<p>We are Finance Corp, We provide Finance consulting from 30 years.</p>
						<p>Core3 is one of the fastest growing International VoIP carrier in Hongkong. offering you a complete portfolio of connections to Europe, North and South-America, Africa, Asia and the Middle-East. Core3 routes millions of international voice minutes from other carriers through state of the art switching platform.</p>
					</div>  
					<div class="col-md-5 col-md-offset-1">
						<img class="no-round" src="images/770.jpg" alt="">
					</div>
				</div>
				
			</div>	
		</div>
	</div>
	<!-- End Content -->

	<!-- Content -->
	<div class="section section-contents section-pad image-on-right bg-light">
		<div class="container">
			<div class="row">
			
				<h5 class="heading-sm-lead">Our Services</h5>
				<h2 class="heading-section">What we do</h2>
				<div class="feature-intro">
					<div class="row">
						<div class="col-sm-7 col-md-6">
							<div class="row">
								<div class="col-sm-6 res-s-bttm">
									<div class="icon-box style-s1 photo-plx-full">
										<em class="fa fa-bar-chart-o" aria-hidden="true"></em>
									</div>
									<h4>Marketing</h4>
									<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
								</div>
								<div class="col-sm-6">
									<div class="icon-box style-s1">
										<em class="fa fa-users" aria-hidden="true"></em>
									</div>
									<h4>Consultation</h4>
									<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
								</div>
								<div class="gaps size-lg"></div>
								<div class="col-sm-6 res-s-bttm">
									<div class="icon-box style-s1">
										<em class="fa fa-credit-card" aria-hidden="true"></em>
									</div>
									<h4>Accounting</h4>
									<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
								</div>
								<div class="col-sm-6">
									<div class="icon-box style-s1">
										<em class="fa fa-trademark" aria-hidden="true"></em>
									</div>
									<h4>Trademarks</h4>
									<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>			
		</div>
		<div class="section-bg imagebg"><img src="images/960.jpg" alt=""></div>
	</div>
	<!-- End Content -->

	<!-- Testimonials -->
	<div class="section section-quotes section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="col-md-offset-2 col-md-8 center">
					<h5 class="heading-sm-lead">The People Say</h5>
					<h2 class="heading-section">Testimonials</h2>
				</div>
				<div class="gaps"></div>
				<div class="testimonials">
					<div id="testimonial" class="quotes-slider col-md-8 col-md-offset-2">
						<div class="owl-carousel loop has-carousel" data-items="1" data-loop="true" data-dots="true" data-auto="true">
							<div class="item">
								<!-- Each Quotes -->
								<div class="quotes">
									<div class="quotes-text center">
										<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo nemo enim ipsam.</p>
									</div>
									<div class="profile">
										<img src="images/160.jpg" alt="">
										<h5>Maria jaqline</h5>
										<h6>CEO, Company Name</h6>
									</div>
								</div>
								<!-- End Quotes -->
							</div>
							<!-- End Slide -->
							<!-- Each Slide -->
							<div class="item">
								<!-- Each Quotes -->
								<div class="quotes">
									<div class="quotes-text center">
										<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</p>
									</div>
									<div class="profile">
										<img src="images/160-2.jpg" alt="">
										<h5>Maria jaqline</h5>
										<h6>CEO, Company Name</h6>
									</div>
								</div>
								<!-- End Quotes -->
							</div>
						</div>
						<!-- End Slide -->
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- End Section -->

	<!-- Content -->
	<div class="section section-contents section-pad has-bg fixed-bg light bg-alternet">
		<div class="container">
			<div class="row">
				
				<div class="row">
					<div class="col-md-4 pad-r res-m-bttm">
						<h2 class="heading-lead">FinanceCorp team provide independent advice based on established research methods.</h2>
					</div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-sm-6 res-s-bttm">
								<div class="icon-box style-s4 photo-plx-full">
									<em class="fa fa-bar-chart-o" aria-hidden="true"></em>
								</div>
								<h4>Experienced</h4>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan.</p>
							</div>
							<div class="col-sm-6">
								<div class="icon-box style-s4">
									<em class="fa fa-users" aria-hidden="true"></em>
								</div>
								<h4>Vibrant</h4>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan.</p>
							</div>
							<div class="gaps size-lg"></div>
							<div class="col-sm-6 res-s-bttm">
								<div class="icon-box style-s4">
									<em class="fa fa-credit-card" aria-hidden="true"></em>
								</div>
								<h4>Professional</h4>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan.</p>
							</div>
							<div class="col-sm-6">
								<div class="icon-box style-s4">
									<em class="fa fa-trademark" aria-hidden="true"></em>
								</div>
								<h4>Trademarks</h4>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan.</p>
							</div>
							</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section-bg imagebg"><img src="images/plx-full.jpg" alt=""></div>
	</div>
	<!-- End Content -->
	<!-- Teams -->
	<div class="section section-teams section-pad bdr-bottom">
		<div class="container">
			<div class="content row">

				<div class="col-md-offset-2 col-md-8 center">
					<h5 class="heading-sm-lead">The Team</h5>
					<h2 class="heading-section">Our Advisors</h2>
				</div>
				<div class="gaps"></div>
				<div class="team-member-row row">
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="images/540.jpg">
							</div>
							<div class="team-info center">
								<h4 class="name">Robert Miller</h4>
								<p class="sub-title">Managing Director &amp; CEO</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="images/540-2.jpg">
							</div>
							<div class="team-info center">
								<h4 class="name">Stephen Everett</h4>
								<p class="sub-title">Chief Financial Officer</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="images/540.jpg">
							</div>
							<div class="team-info center">
								<h4 class="name">Philip Hennessy</h4>
								<p class="sub-title">Senior Tax Specialist</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="images/540-2.jpg">
							</div>
							<div class="team-info center">
								<h4 class="name">Robert Miller</h4>
								<p class="sub-title">Chief Financial Advisor</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
				</div><!-- TeamRow #end -->
			</div>
		</div>
	</div>			
	<!-- End Section -->
	<!-- Latest News -->
	<div class="section section-news section-pad">
		<div class="container">
			<div class="content row">
				
				<h5 class="heading-sm-lead center">Latest News</h5>
				<h2 class="heading-section center">Our Financial Updates</h2>

				<div class="row">
					<!-- Blog Post Loop -->
					<div class="blog-posts">
						<div class="post post-loop  col-md-4">
							<div class="post-thumbs">
								<a href="news-details.html"><img alt="" src="images/740.jpg"></a>
							</div>
							<div class="post-entry">
								<div class="post-meta"><span class="pub-date">15, Aug 2017</span></div>
								<h2><a href="news-details.html">Income Increase Shows the Recovery Is Very Much Real</a></h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...</p>
								<a class="btn btn-alt" href="#">Read More</a>
							</div>
						</div>
						<div class="post post-loop col-md-4">
							<div class="post-thumbs">
								<a href="news-details.html"><img alt="" src="images/740-2.jpg"></a>
							</div>
							<div class="post-entry">
								<div class="post-meta"><span class="pub-date">04, Jul 2017</span></div>
								<h2><a href="news-details.html">An Economics Nobel awarded for Examining Reality</a></h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...</p>
								<a class="btn btn-alt" href="#">Read More</a>
							</div>
						</div>
						<div class="post post-loop col-md-4">
							<div class="post-thumbs">
								<a href="news-details.html"><img alt="" src="images/740-3.jpg"></a>
							</div>
							<div class="post-entry">
								<div class="post-meta"><span class="pub-date">26, Dec 2016</span></div>
								<h2><a href="news-details.html">Maybe Supply-Side Economics Deserves a Second Look</a></h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...</p>
								<a class="btn btn-alt" href="#">Read More</a>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- End Section -->
	
	<!-- Client logo -->
	