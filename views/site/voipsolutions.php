<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<h1 class="page-title">OUR VOIP SOLUTION</h1>
							<p>Core3 offers Domestic & International termination services and direct routes for Telecom operators, resellers and companies.</p>						
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="index.html">Home</a></li>
								<li class="active"><span>Our Services</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="/images/192012.jpg" alt="" />
			</div>
		</div>
		<!-- #end Banner/Static -->
	</header>
	<!-- End Header -->
	<!-- Content -->
	<div class="section section-content section-pad bg-light">
		<div class="container">
			<div class="row">
				
				<div class="row row-vm reverses">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/770.jpg" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">VOIP SOLUTION</h2>
						<p class="just">Core3 is one of the fastest growing International VoIP carrier in Hongkong.Core3 offers a complete portfolio of connections to Europe, North and South-America, Africa, Asia and the Middle-East. Core3 routes millions of international voice minutes from other carriers through state of the art switching platform. 
Core3 is known as the most aggressive, flexible and quality-driven company in the wholesale arena; we are always searching for VoIP and SS7-carriers offering good quality routes at the best tariffs available. Core3 maintains long term relationships with PTT's, incumbents and mobile operators from all over the world. We have interconnected with various global network providers through our points of presence and we are continually connecting more and more voice carriers to meet the demand of our partners.
</p>
						<!-- <p><a href="#" class="btn btn-outline">More about Services</a></p> -->
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/770.jpg" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Financial Planning</h2>
						<p class="lead">Our financial planning expertise covers eiusmod tempor dunt ut labore et dolore mane.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nost.</p>
						<p><a href="#" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad bg-light">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm reverses">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/770.jpg" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Investments Management</h2>
						<p class="lead">We are responsible sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nost.</p>
						<p><a href="#" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/770.jpg" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Insurance Consulting</h2>
						<p class="lead">We provide best consulting sed do eiusmod tempor incididunt ut labore et dolore.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nost.</p>
						<p><a href="#" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad bg-light">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm reverses">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/770.jpg" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Taxes consulting</h2>
						<p class="lead">Taxes consulting includes sed do eiusmod tempor incididunt ut labore et dolore.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nost.</p>
						<p><a href="#" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/770.jpg" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Business Audit</h2>
						<p class="lead">We are specializes in business audit eiusmod tempor idunt labore et dolore.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nost.</p>
						<p><a href="#" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- End Content -->	
	<!-- Client logo -->
	