<?php 
    use yii\helpers\Html;
    use \app\helpers\ImageUploader;
    $propertyHelper = new \app\helpers\PropertyHelper();
    $sector15 = $propertyHelper::getDataByLocalityName('Sector 15','Gurugram');
    $sector14 = $propertyHelper::getDataByLocalityName('Sector 14','Gurugram');
    $patelNagar = $propertyHelper::getDataByLocalityName('Patel Nagar','Gurugram');
    $rajivNagar = $propertyHelper::getDataByLocalityName('Sector 13','Gurugram');
    $gurugram = $propertyHelper::getDataByCityName('Gurugram');
?>
 <div class="navigation">
    <div class="row hidden-xs ">
        <div class="" style="">
        <nav class="navbar navbar-xs navbar-default">
            <div class="collapse navbar-collapse js-navbar-collapse" >
            <ul class="nav navbar-nav" style="padding-bottom: -0.3em!important;">
            <li class="dropdown mega-dropdown menu__item" id="mega-dropdown">
                <a href="/search/search-result?location=Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=girls&city_name=Gurugram&lat=28.4594965&long=77.02663830000006&pincode=&state_name=Haryana&locality=&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=Zi5tN2ZBQVFXdCl0DjcFaBZkPg82dQYWJxkFQi0RJzIWYglBFiJ3MA%3D%3D" target="_blank" class="dropdown-toggle" data-toggle="dropdown" style="padding-left: 3em;">Sector 15</a>

               <ul class="dropdown-menu mega-dropdown-menu row" id="mega-drop" style=" margin-top: -0.1em;">
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">New in Sector 15</li>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                        <?php 
                        $i = 0;
                        foreach($sector15 as $secProp){
                            $propertyImages = $secProp->propertyImages;
                            ?>
                                <div class="item <?= ($i=='0') ? 'active' : ''?>">
                                <a href="#"> <?php 
                                if(!empty($propertyImages))
                                {
                                echo Html::img(ImageUploader::resizeRender($propertyImages[0]->url, '254', '150'),['class' => 'img-responsive','alt' => 'Property on rent in Gurugram sector 15']);
                                }
                                else
                                {
                                   echo Html::img(ImageUploader::resizeRender('/images/not-found.png', '254', '150'),['class' => 'img-responsive','alt' => 'OSMSTAYS Rental Service']);
                                }
                                ?>
                                </a>
                                <h4><small><?= $secProp->property_name;?></small></h4>
                                 <a target="_blank" class="btn btn-warning vik" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($secProp->property_name);?>"> Visit Property >></a>
                                <!-- <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Add to Wishlist</button> -->
                                </div>
                            <?php 
                                $i++;
                            }?>
                        </div>
                        
                        <li><a href="/search/search-result?location=Sector+15%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4621359&long=77.04637189999994&pincode=122022&state_name=Haryana&locality=Sector+15&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=YWxySE1XXy44NEU9dAU0FhgHNWV6PBV4FyY3HgQ4LEdUH0stfzsNHQ%3D%3D">View all Location <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Premium Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-133">OSM PG 133</a></li>
                        <li><a href="/property/property-details?name=osm-pg-142">OSM PG 142</a></li>
                        <li><a href="/property/property-details?name=osm-pg-156">OSM PG 156</a></li>
                                             
                        
                        <li class="dropdown-header">Girls - Premium Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-130">OSM PG 130</a></li>
                        <li><a href="/property/property-details?name=osm-pg-143">OSM PG 143</a></li>
                        <li><a href="/property/property-details?name=osm-pg-157">OSM PG 157</a></li>
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Flagship Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-158">OSM PG 158</a></li>
                        <li><a href="/property/property-details?name=osm-pg-008">OSM PG 008</a></li>
                        <li><a href="/property/property-details?name=osm-pg-077">OSM PG 077</a></li>
                       
                        
                        <li class="dropdown-header">Girls - Flagship Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-184">OSM PG 184</a></li>
                        <li><a href="/property/property-details?name=osm-pg-037">OSM PG 037</a></li>
                        <li><a href="/property/property-details?name=osm-pg-159">OSM PG 159</a></li>
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Economy Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-078">OSM PG 078</a></li>
                        <li><a href="/property/property-details?name=osm-pg-079">OSM PG 079</a></li>
                        <li><a href="/property/property-details?name=osm-pg-040">OSM PG 040</a></li>
                       
                        
                       <li class="dropdown-header">Girls - Economy Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-183">OSM PG 183</a></li>
                         <li><a href="/property/property-details?name=osm-pg-185">OSM PG 185</a></li>
                         <li><a href="/property/property-details?name=osm-pg-053">OSM PG 053</a></li>
                       </ul>
                    </li>
                </ul>
                </li>
                <!-- <li class="menu__item"><a href="/" class="menu__link">Noida</a></li> -->
                <!-- <li class="menu__item"><a href="/" class="menu__link">Faridabad</a></li> -->
            </ul>
            <ul class="nav navbar-nav">
            <li class="dropdown mega-dropdown menu__item" id="mega-dropdown">
                <a href="/search/search-result?location=Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=girls&city_name=Gurugram&lat=28.4594965&long=77.02663830000006&pincode=&state_name=Haryana&locality=&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=Zi5tN2ZBQVFXdCl0DjcFaBZkPg82dQYWJxkFQi0RJzIWYglBFiJ3MA%3D%3D" target="_blank" class="dropdown-toggle" data-toggle="dropdown">Sector 14</a>

               <ul class="dropdown-menu mega-dropdown-menu row" id="mega-drop" style=" margin-top: -0.1em">
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">New in Sector 14</li>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                           <?php 
                        $i = 0;
                        foreach($sector14 as $secProp){
                            $propertyImages = $secProp->propertyImages;
                            ?>
                                <div class="item <?= ($i=='0') ? 'active' : ''?>">
                                <a href="#"> <?php 
                                if(!empty($propertyImages))
                                {
                                echo Html::img(ImageUploader::resizeRender($propertyImages[0]->url, '254', '150'),['class' => 'img-responsive','alt' => 'Property on rent in Gurugram sector 14']);
                                }
                                else
                                {
                                   echo Html::img(ImageUploader::resizeRender('/images/not-found.png', '254', '150'),['class' => 'img-responsive','alt' => 'OSMSTAYS Rental Service']);
                                }
                                ?>
                                </a>
                                <h4><small><?= $secProp->property_name;?></small></h4>
                                 <a target="_blank" class="btn btn-warning vik" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($secProp->property_name);?>"> Visit Property >></a>
                                </div>
                            <?php 
                                $i++;
                            }?>
                        </div>

                        
                        <li><a href="search/search-result?location=Sector+14%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.47074169999999&long=77.04637189999994&pincode=122022&state_name=Haryana&locality=Sector+14&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=N25FYzZYOFBuNnIWDwpTaE4FAk4BM3IGQSQANX83SzkCHXwGBDRqYw%3D%3D">View all Location <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Premium Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-151">OSM PG 151</a></li>
                        <li><a href="/property/property-details?name=osm-pg-017">OSM PG 017</a></li>
                        <li><a href="/property/property-details?name=osm-pg-075">OSM PG 075</a></li>
                       
                        

                         <li class="dropdown-header">Girls- Premium Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-002">OSM PG 002</a></li>
                        <li><a href="/property/property-details?name=osm-pg-049">OSM PG 049</a></li>
                        <li><a href="/property/property-details?name=osm-pg-114">OSM PG 114</a></li> 
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Flagship Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-023">OSM PG 023</a></li>
                        <li><a href="/property/property-details?name=osm-pg-072">OSM PG 072</a></li>
                        <li><a href="/property/property-details?name=osm-pg-076">OSM PG 076</a></li>
                       
                        
                         <li class="dropdown-header">Girls - Flagship Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-116">OSM PG 116</a></li>
                        <li><a href="/property/property-details?name=osm-pg-117">OSM PG 117</a></li>
                        <li><a href="/property/property-details?name=osm-pg-150">OSM PG 150</a></li> 
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Economy Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-046">OSM PG 046</a></li>
                        <li><a href="/property/property-details?name=osm-pg-073">OSM PG 073</a></li>
                        <li><a href="/property/property-details?name=osm-pg-112">OSM PG 112</a></li>

                        
                        <li class="dropdown-header">Girls - Economy Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-009">OSM PG 009</a></li>
                        <li><a href="/property/property-details?name=osm-pg-074">OSM PG 074</a></li>
                        <li><a href="/property/property-details?name=osm-pg-048">OSM PG 048</a></li>
                    </ul>
                    </li>
                </ul>
                </li>
                <!-- <li class="menu__item"><a href="/" class="menu__link">Noida</a></li> -->
                <!-- <li class="menu__item"><a href="/" class="menu__link">Faridabad</a></li> -->
            </ul><ul class="nav navbar-nav">
            <li class="dropdown mega-dropdown menu__item" id="mega-dropdown">
                <a href="/search/search-result?location=Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=girls&city_name=Gurugram&lat=28.4594965&long=77.02663830000006&pincode=&state_name=Haryana&locality=&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=Zi5tN2ZBQVFXdCl0DjcFaBZkPg82dQYWJxkFQi0RJzIWYglBFiJ3MA%3D%3D" target="_blank" class="dropdown-toggle" data-toggle="dropdown">Patel Nagar</a>

               <ul class="dropdown-menu mega-dropdown-menu row" id="mega-drop" style=" margin-top: -0.1em">
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">New in Patel Nagar</li>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                             <?php 
                        $i = 0;
                        foreach($patelNagar as $secProp){
                            $propertyImages = $secProp->propertyImages;
                            ?>
                                <div class="item <?= ($i=='0') ? 'active' : ''?>">
                                <a href="#"> <?php 
                                if(!empty($propertyImages))
                                {
                                echo Html::img(ImageUploader::resizeRender($propertyImages[0]->url, '254', '150'),['class' => 'img-responsive','alt' => 'Property on rent in Patel Nagar, Gurugram ']);
                                }
                                else
                                {
                                   echo Html::img(ImageUploader::resizeRender('/images/not-found.png', '254', '150'),['class' => 'img-responsive','alt' => 'OSMSTAYS Rental Service']);
                                }
                                ?>
                                </a>
                                <h4><small><?= $secProp->property_name;?></small></h4>
                                 <a target="_blank" class="btn btn-warning vik" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($secProp->property_name);?>"> Visit Property >></a>
                                </div>
                            <?php 
                                $i++;
                            }?>                        
                            </div>
                        
                        <li><a href="/search/search-result?location=Patel+Nagar%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4605963&long=77.03959600000007&pincode=122001&state_name=Haryana&locality=Patel+Nagar&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=LVVJUWxLNER0DX4kVRlffFQ.DnxbIH4SWx8MByUkRy0YJnA0Xidmdw%3D%3D">View all Location <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Premium Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-057">OSM PG 057</a></li>
                        <li><a href="/property/property-details?name=osm-pg-152">OSM PG 152</a></li>
                        <li><a href="/property/property-details?name=osm-pg-153">OSM PG 153</a></li>
                        
                        
                            
                         <li class="dropdown-header">Girls- Premium Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-235">OSM PG 235</a></li>
                        <li><a href="/property/property-details?name=osm-pg-223">OSM PG 223</a></li>
                        <li><a href="/property/property-details?name=osm-pg-038">OSM PG 038</a></li> 
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Flagship Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-154">OSM PG 154</a></li>
                        <li><a href="/property/property-details?name=osm-pg-155">OSM PG 155</a></li>
                        <li><a href="/property/property-details?name=osm-pg-176">OSM PG 176</a></li>
                       
                        
                         <li class="dropdown-header">Girls - Flagship Homes</li>
                         <li><a href="/property/property-details?name=osm-pg-041">OSM PG 041</a></li>
                         <li><a href="/property/property-details?name=osm-pg-085">OSM PG 085</a></li>
                         <li><a href="/property/property-details?name=osm-pg-167">OSM PG 167</a></li>
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys- Economy Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-177">OSM PG 177</a></li>
                        <li><a href="/property/property-details?name=osm-pg-080">OSM PG 080</a></li>
                        <li><a href="/property/property-details?name=osm-pg-012">OSM PG 012</a></li>
                        
                        <li class="dropdown-header">Girls - Economy Homes</li>
                      <li><a href="/property/property-details?name=osm-pg-222">OSM PG 222</a></li>
                        <li><a href="/property/property-details?name=osm-pg-084">OSM PG 084</a></li>
                        <li><a href="/property/property-details?name=osm-pg-003">OSM PG 003</a></li>
                    </ul>
                    </li>
                </ul>
                </li>
                <!-- <li class="menu__item"><a href="/" class="menu__link">Noida</a></li> -->
                <!-- <li class="menu__item"><a href="/" class="menu__link">Faridabad</a></li> -->
            </ul><ul class="nav navbar-nav">
            <li class="dropdown mega-dropdown menu__item" id="mega-dropdown">
                <a href="/search/search-result?location=Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=girls&city_name=Gurugram&lat=28.4594965&long=77.02663830000006&pincode=&state_name=Haryana&locality=&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=Zi5tN2ZBQVFXdCl0DjcFaBZkPg82dQYWJxkFQi0RJzIWYglBFiJ3MA%3D%3D" target="_blank" class="dropdown-toggle" data-toggle="dropdown">Rajiv Nagar</a>

               <ul class="dropdown-menu mega-dropdown-menu row" id="mega-drop" style=" margin-top: -0.1em">
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">New in Rajiv Nagar</li>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">

                        <?php 
                        $i = 0;
                        foreach($rajivNagar as $secProp){
                            $propertyImages = $secProp->propertyImages;
                            ?>
                                <div class="item <?= ($i=='0') ? 'active' : ''?>">
                                <a href="#"> <?php 
                                if(!empty($propertyImages))
                                {
                                echo Html::img(ImageUploader::resizeRender($propertyImages[0]->url, '254', '150'),['class' => 'img-responsive','alt' => 'Property on rent in Rajiv Nagar, Gurugram ']);
                                }
                                else
                                {
                                   echo Html::img(ImageUploader::resizeRender('/images/not-found.png', '254', '150'),['class' => 'img-responsive','alt' => 'OSMSTAYS Rental Service']);
                                }
                                ?>
                                </a>
                                <h4><small><?= $secProp->property_name;?></small></h4>
                                 <a target="_blank" class="btn btn-warning vik" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($secProp->property_name);?>"> Visit Property >></a>
                                </div>
                            <?php 
                                $i++;
                            }?>                   
                        </div>                        
                        <li><a href="/search/search-result?location=Sector+13%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4759235&long=77.04061469999999&pincode=122022&state_name=Haryana&locality=Sector+13&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=ZUZNVVZQUnI8HnogbwI5ShwtCnhhOxgkEwwIAx8%2FIRtQNXQwZDwAQQ%3D%3D">View all Location <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Premium Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-015">OSM PG 015</a></li>
                        <li><a href="/property/property-details?name=osm-pg-044">OSM PG 044</a></li>
                        <li><a href="/property/property-details?name=osm-pg-056">OSM PG 056</a></li>
                      
                        

                        <li class="dropdown-header">Girls- Premium Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-134">OSM PG 134</a></li>
                        <li><a href="/property/property-details?name=osm-pg-138">OSM PG 138</a></li>
                        <li><a href="/property/property-details?name=osm-pg-149">OSM PG 149</a></li>
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys - Flagship Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-081">OSM PG 081</a></li>
                        <li><a href="/property/property-details?name=osm-pg-165">OSM PG 165</a></li>
                        <li><a href="/property/property-details?name=osm-pg-004">OSM PG 004</a></li>

                        
                        <li class="dropdown-header">Girls - Flagship Homes</li>
                         <li><a href="/property/property-details?name=osm-pg-022">OSM PG 022</a></li>
                        <li><a href="/property/property-details?name=osm-pg-094">OSM PG 094</a></li>
                        <li><a href="/property/property-details?name=osm-pg-199">OSM PG 199</a></li>
 
                    </ul>
                    </li>
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">Boys- Economy Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-050">OSM PG 050</a></li>
                        <li><a href="/property/property-details?name=osm-pg-106">OSM PG 106</a></li>
                        <li><a href="/property/property-details?name=osm-pg-124">OSM PG 124</a></li>
                       
                        
                        <li class="dropdown-header">Girls- Economy Homes</li>
                        <li><a href="/property/property-details?name=osm-pg-091">OSM PG 091</a></li>
                        <li><a href="/property/property-details?name=osm-pg-024">OSM PG 024</a></li>
                        <li><a href="/property/property-details?name=osm-pg-144">OSM PG 144</a></li>
                    </ul>
                    </li>
                </ul>
                </li>
                <!-- <li class="menu__item"><a href="/" class="menu__link">Noida</a></li> -->
                <!-- <li class="menu__item"><a href="/" class="menu__link">Faridabad</a></li> -->
            </ul>
            <ul class="nav navbar-nav">
            <li class="dropdown mega-dropdown menu__item" id="mega-dropdown">
                <a href="/search/search-result?location=Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=girls&city_name=Gurugram&lat=28.4594965&long=77.02663830000006&pincode=&state_name=Haryana&locality=&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=Zi5tN2ZBQVFXdCl0DjcFaBZkPg82dQYWJxkFQi0RJzIWYglBFiJ3MA%3D%3D" target="_blank" class="dropdown-toggle" data-toggle="dropdown">All Localities</a>

               <ul class="dropdown-menu mega-dropdown-menu row" id="mega-drop" style=" margin-top: -0.1em">
                    <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">New in Gurugram</li>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                            <?php 
                            $i = 0;
                            foreach($gurugram as $secProp){
                                $propertyImages = $secProp->propertyImages;
                                ?>
                                    <div class="item <?= ($i=='0') ? 'active' : ''?>">
                                    <a href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($secProp->property_name);?>"> <?php 
                                    if(!empty($propertyImages))
                                    {
                                    echo Html::img(ImageUploader::resizeRender($propertyImages[0]->url, '254', '150'),['class' => 'img-responsive','alt' => 'Property on rent in Rajiv Nagar, Gurugram ']);
                                    }
                                    else
                                    {
                                       echo Html::img(ImageUploader::resizeRender('/images/not-found.png', '254', '150'),['class' => 'img-responsive','alt' => 'OSMSTAYS Rental Service']);
                                    }
                                    ?>
                                    </a>
                                    <h4><small><?= $secProp->property_name;?></small></h4>
                                     <a target="_blank" class="btn btn-warning vik" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($secProp->property_name);?>"> Visit Property >></a>
                                    </div>
                                <?php 
                                    $i++;
                                }?>    
                            </div>
                        

                        
                        <li><a href="/search/search-result?location=Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4594965&long=77.02663830000006&pincode=&state_name=Haryana&locality=&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=LU45RGMwLmcVPwsuK1sbJBgMdSIXYmkzdSJJABQdQCwbPAlpD29kIQ%3D%3D">View all Location <span class="glyphicon glyphicon-chevron-right"></span></a></li></div>
                    </ul>
                    </li>
                   
                    <li class="col-sm-2">
                    <ul>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+4%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4739868&long=77.01150080000002&pincode=122001&state_name=Haryana&locality=Sector+4&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-4</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+5%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4794366&long=77.01758319999999&pincode=122022&state_name=Haryana&locality=Sector+5&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-5</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+12%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4748793&long=77.03258840000001&pincode=122001&state_name=Haryana&locality=Sector+12&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-12</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+13%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4759235&long=77.04061469999999&pincode=122022&state_name=Haryana&locality=Sector+13&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-13</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+14%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.47074169999999&long=77.04637189999994&pincode=122022&state_name=Haryana&locality=Sector+14&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-14</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+14%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.47074169999999&long=77.04637189999994&pincode=122022&state_name=Haryana&locality=Sector+14&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-15</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+17%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4791609&long=77.05932459999997&pincode=122022&state_name=Haryana&locality=Sector+17&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-17</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+18%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.49450449999999&long=77.06928270000003&pincode=122022&state_name=Haryana&locality=Sector+18&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-18</a></li>
                        
                        
                         <li><a href="http://osmstays.com/search/search-result?location=Sector+21%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.5133929&long=77.07227590000002&pincode=&state_name=Haryana&locality=Sector+21&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-21</a></li>                       
                       
                    </ul>
                    </li>
                    <li class="col-sm-2">
                    <ul>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+22%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.507241&long=77.06404859999998&pincode=122015&state_name=Haryana&locality=Sector+22&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-22</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+23%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.5110641&long=77.0630248&pincode=&state_name=Haryana&locality=Sector+23&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-23</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+27%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4657793&long=77.09170029999996&pincode=122009&state_name=Haryana&locality=Sector+27&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-27</a></li>

                         <li><a href="http://osmstays.com/search/search-result?location=Sector+30%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4603762&long=77.0578855&pincode=122022&state_name=Haryana&locality=Sector+30&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-30</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+31%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.453807&long=77.0492504&pincode=122001&state_name=Haryana&locality=Sector+31&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-31</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+33%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4388158&long=77.02765999999997&pincode=122022&state_name=Haryana&locality=Sector+33&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-33</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+38%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4357613&long=77.04061469999999&pincode=122022&state_name=Haryana&locality=Sector+38&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-38</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+39%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4477547&long=77.0531714&pincode=122022&state_name=Haryana&locality=Sector+39&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-39</a></li>

                        <li><a href="http://osmstays.com/search/search-result?location=Sector+40%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4498494&long=77.0566887&pincode=122001&state_name=Haryana&locality=Sector+40&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-40</a></li>
                    </ul>
                    </li>
                    <li class="col-sm-2">
                    <ul>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+41%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4567655&long=77.0650809&pincode=122022&state_name=Haryana&locality=Sector+41&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-41</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+43%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4629171&long=77.0772455&pincode=122022&state_name=Haryana&locality=Sector+43&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-43</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+45%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4452895&long=77.0650809&pincode=122022&state_name=Haryana&locality=Sector+45&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-45</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+46%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4345552&long=77.0578855&pincode=122022&state_name=Haryana&locality=Sector+46&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-46</a></li>
                       
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+51%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.429647&long=77.06651999999997&pincode=122003&state_name=Haryana&locality=Sector+51&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-51</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+52%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4354721&long=77.08234820000007&pincode=122003&state_name=Haryana&locality=Sector+52&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-52</a></li>
                            
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+53%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4396185&long=77.09715269999992&pincode=&state_name=Haryana&locality=Sector+53&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-53</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+55%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4280506&long=77.10968290000005&pincode=&state_name=Haryana&locality=Sector+55&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-55</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=Sector+56%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4262171&long=77.096765&pincode=&state_name=Haryana&locality=Sector+56&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=V29Yc1UxQ3UCX2A9EV8mOQMHNAA4eRotZyYZMBpicDoUCB0KZUB6Fg%3D%3D">Sector-56</a></li>

                                 
                    </ul>
                    </li>
                    <li class="col-sm-2">
                    <ul>

                        <li><a href="http://osmstays.com/search/search-result?location=DLF+Phase+1%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4650453&long=77.10071169999992&pincode=122002&state_name=Haryana&locality=Sector+26&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=UlUuQjhNNmUHZRYMfCNTKQY9QjFVBW89YhxvAXceBSoRMms7CDwPBg%3D%3D">DLF Phase 1</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=DLF+Phase+2%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.48766669999999&long=77.08810340000002&pincode=122022&state_name=Haryana&locality=Sector+25&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=UlUuQjhNNmUHZRYMfCNTKQY9QjFVBW89YhxvAXceBSoRMms7CDwPBg%3D%3D">DLF Phase 2</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=DLF+Phase+3%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4916812&long=77.09489699999995&pincode=&state_name=Haryana&locality=Sector+24&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=UlUuQjhNNmUHZRYMfCNTKQY9QjFVBW89YhxvAXceBSoRMms7CDwPBg%3D%3D">DLF Phase 3</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=DLF+Phase+4%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.46466989999999&long=77.08448010000006&pincode=122022&state_name=Haryana&locality=&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=UlUuQjhNNmUHZRYMfCNTKQY9QjFVBW89YhxvAXceBSoRMms7CDwPBg%3D%3D">DLF Phase 4</a></li>
                        <li><a href="http://osmstays.com/search/search-result?location=DLF+Phase+5%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4476911&long=77.09477849999996&pincode=&state_name=Haryana&locality=Sector+43&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=UlUuQjhNNmUHZRYMfCNTKQY9QjFVBW89YhxvAXceBSoRMms7CDwPBg%3D%3D">DLF Phase 5</a></li>
                        <li><a href="/search/search-result?location=Patel+Nagar%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4605963&long=77.03959600000007&pincode=122001&state_name=Haryana&locality=Patel+Nagar&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=LVVJUWxLNER0DX4kVRlffFQ.DnxbIH4SWx8MByUkRy0YJnA0Xidmdw%3D%3D">Patel Nagar</a></li>
                        <li><a href="/search/search-result?location=Sector+13%2C+Gurugram%2C+Haryana%2C+India&rooms_type=&rooms_for=&city_name=Gurugram&lat=28.4759235&long=77.04061469999999&pincode=122022&state_name=Haryana&locality=Sector+13&tag_premium=premium&tag_flagship=flagship&tag_economy=economy&fullyfur=furnished&semifur=semifurnished&unfur=unfurnished&page_name=home&displayview=grid&_csrf=ZUZNVVZQUnI8HnogbwI5ShwtCnhhOxgkEwwIAx8%2FIRtQNXQwZDwAQQ%3D%3D">Rajiv Nagar</a></li>
                        
                        
                    </ul>
                    </li>
                </ul>
                </li>
                <!-- <li class="menu__item"><a href="/" class="menu__link">Noida</a></li> -->
                <!-- <li class="menu__item"><a href="/" class="menu__link">Faridabad</a></li> -->
            </ul>

                    <div class="social-icons" style="padding-right: 2em;">
                    <a target="_blank" href="https://www.facebook.com/OSMSTAYS-752000994997438/" target="_blank"><i class="icon"></i></a>
                    <a target="_blank" href="https://twitter.com/osmstays"><i class="icon1"></i></a>
                    <a target="_blank" href="https://plus.google.com/102988358230820191177"><i class="icon2"></i></a>
                    <a target="_blank" href="https://www.linkedin.com/company/11290431/"><i class="icon3"></i></a>
                </div>
                <div class="pull-right" style="padding-right: 10px; margin-top: 3px;"><a href="https://www.instagram.com/osmstays/" target="_blank" style="padding: none!important; margin: none!important;"><i class="fa fa-instagram  insta" aria-hidden="true"></i></a></div>

                </div>
            </div>
        </nav>
      </div>
    </div>
</div>
