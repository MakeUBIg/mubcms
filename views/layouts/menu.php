<?php 

$menu = [];
if(\Yii::$app->user->can('dashboard/index'))
{
    $menu[] = ["label" => "Dashboard", "url" => "/mub-admin", "icon" => "home"];        
}


if(\Yii::$app->user->can('album/index'))
{
$menu[] = [
            "label" => "Gallery", 
            "url" => ["/mub-admin/gallery"], 
            "icon" => "picture-o"
        ];
}
if(\Yii::$app->user->can('user/index'))
{
     $userss = new  app\models\MubUser();

    $newCount = $userss::find()->where(['del_status' => '0','status' => 'inactive'])->count();
    $userssCount = $userss::find()->where(['del_status' => '0','status' => 'active'])->count();
    $menu[] = ["label" => "Users", "url" => ["/mub-admin/users"], "icon" => "user",
                "badge" => ($newCount > 0) ? $newCount : $userssCount,
                "badgeOptions" => ["class" => ($newCount > 0) ?"label-success" : ''],
                "icon" => "cog"
            ];
}

if(\Yii::$app->user->can('user/index'))
{
$menu[] = ["label" => "Front-end-Users", "url" => ["/mub-admin/users/user/frontend-user"], "icon" => "user"];
}

if(\Yii::$app->user->can('property/index'))
{
    $mubUserId = \app\models\User::getMubUserId();
    $userRole = \Yii::$app->controller->getUserRole();
    if($userRole == 'admin')
    {
    $states = new \app\models\State();
    $active = $states::find()->where(['active' => '1'])->all();
    $submenu = [];
    $property = new \app\modules\MubAdmin\modules\RealEstate\models\Property();

    foreach ($active as $key => $activeState) {
        $inactiveProp = intval($property::find()->where(['state_id' => $activeState->id,'status' => 'inactive'])->count());
        $activeProp = intval($property::find()->where(['state_id' => $activeState->id,'status' => 'active'])->count());
        $submenu[] =[
          "label" => $activeState->state_name,
          "url" => "/mub-admin/real-estate/property?state=".$activeState->id,
          'ptions'=> ['class'=>'displayblock'],
          "badge" => ($inactiveProp > 0) ? $inactiveProp : $activeProp,
          "badgeOptions" => ["class" => ($inactiveProp > 0) ? "label-success" : '']
          ];
        }
    }
    else
    {
    $property = new \app\modules\MubAdmin\modules\RealEstate\models\Property();
    $userStates = new \app\models\UserStates();
    $user = new \app\models\User();
    $mubUserId = $user::getMubUserId();
    $states = new \app\models\State();
    $mubUserStates = $userStates::find()->where(['mub_user_id' => $mubUserId])->all();
    $submenu = [];
    foreach ($mubUserStates as $key => $activeState) {
        $currentState = $states::findOne($activeState->state_id);
         $inactiveProp = \intval($property::find()->where(['state_id' => $activeState->id,'mub_user_id' => $mubUserId,'status' => 'inactive'])->count());
        $activeProp = \intval($property::find()->where(['state_id' => $activeState->id,'mub_user_id' => $mubUserId,'status' => 'active'])->count());
        $submenu[] =[
          "label" => $currentState->state_name,
          "url" => "/mub-admin/real-estate/property?state=".$currentState->id,
          "badge" => ($inactiveProp > 0) ? $inactiveProp : $activeProp,
          "badgeOptions" => ["class" => ($inactiveProp > 0) ? "label-success" : '']
          ];
        }

    }
$menu[] = [
		"label" => "Real Estate",
        "url" => "#",
        "icon" => "home",
        'options'=> ['class'=>'displayblock'],
        "items" => [
            [
                "label" => "States",
                "url" => "#",
                "items" => $submenu
            ],
        ],];

if(\Yii::$app->user->can('amenity/index'))
{
    $amenities = new  app\modules\MubAdmin\modules\RealEstate\models\Amenity();

    $newCOunt = $amenities::find()->where(['del_status' => '0','status' => 'inactive'])->count();
    $amenityCount = $amenities::find()->where(['del_status' => '0','status' => 'active'])->count();
    $menu[] = [
                "label" => "Amenities",
                "url" => ["/mub-admin/real-estate/amenity"],
                "badge" => ($newCOunt > 0) ? $newCOunt : $amenityCount,
                "badgeOptions" => ["class" => ($newCOunt > 0) ?"label-success" : ''],
                "icon" => "cog"
            ];
}

if(\Yii::$app->user->can('support-staff/index'))
{
    $supportStaff = new  app\modules\MubAdmin\modules\RealEstate\models\PropertySupportStaff();
    $supportStaffCount = $supportStaff::find()->where(['del_status' => '0','mub_user_id' => $mubUserId])->count();
    $menu[] = [
                "label" => "Support Staff",
                "url" => ["/mub-admin/real-estate/support-staff"],
                "badge" => $supportStaffCount,
                "badgeOptions" => ["class" => ($supportStaffCount=='0') ? "label-danger" : "label-warning"],
                "icon" => "life-ring"
            ];
}

if(\Yii::$app->user->can('tenant/index'))
{
    $tenant = new  app\modules\MubAdmin\modules\RealEstate\models\Tenant();
    $tenantCount = $tenant::find()->where(['del_status' => '0','mub_user_id' => $mubUserId])->count();
    $menu[] = [
                "label" => "Tenant",
                "url" => ["/mub-admin/real-estate/tenant"],
                "badge" => $tenantCount,
                "badgeOptions" => ["class" => ($supportStaffCount=='0') ? "label-danger" : "label-warning"],
                "icon" => "users"
            ];
}

if(\Yii::$app->user->can('property/index'))
{
    $menu[] = [
                "label" => "Your Profile",
                "url" => ["/mub-admin/real-estate/property/profile"],
                "icon" => "user"
            ];
}

}
?>
