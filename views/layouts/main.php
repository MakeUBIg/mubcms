<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\MubUser;

AppAsset::register($this);

 $this->registerMetaTag([
      'title' => 'og:title',
      'content' =>'OSMSTAYS Property', 
   ]);

   $this->registerMetaTag([
      'app_id' => 'fb:app_id',
      'content' => '892362904261944'
   ]);

   $this->registerMetaTag([
      'type' => 'og:type',
      'content' => 'article'
   ]);

   $this->registerMetaTag([
      'url' => 'og:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
      $this->registerMetaTag([
         'image' => 'og:image',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo.jpg',
      ]);
   $this->registerMetaTag([
      'description' => 'og:description',
      'content' => 'Looking for a house on rent in Gurgoan? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.'
   ]);

/*close facebook meta tag*/

/*twitter meta tag*/

   $this->registerMetaTag([
      'card' => 'twitter:card',
      'content' => "summury"
   ]);
   $this->registerMetaTag([
      'site' => "twitter:site",
      'content' => "@publisher_handle"
   ]);
   
   $this->registerMetaTag([
      'title' => 'twitter:title',
      'content' => 'OSMSTAYS Property'
   ]);

   $this->registerMetaTag([
      'description' => 'twitter:description',
      'content' => 'Looking for a house on rent in Gurgoan? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.' 
   ]);

   $this->registerMetaTag([
      'creater' => 'twitter:creater',
      'content' => '@author_handle' 
   ]);

   $this->registerMetaTag([
      'url' => 'twitter:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
   $this->registerMetaTag([
         'image' => 'twitter:image:src',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo.jpg',
      ]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="zxx">
<head>
  <title>Core3 Network Private Limited</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="icon" href="/images/favicon.png" type="/images/png" sizes="16x16">
  <?= Html::csrfMetaTags() ?>
   <?php $this->head() ?>
</head>

<body class="site-body style-v1">
  <!-- Header --> 
  <header class="site-header header-s1 is-transparent is-sticky">
    <!-- Topbar -->
    <div class="topbar">
      <div class="container">
        <div class="row">
          <div class="top-aside top-left">
            <ul class="top-nav">
              <li><a href="#">On Press</a></li>
              <li><a href="#">Career</a></li>
              <li><a href="#">Our Offices</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
          <div class="top-aside top-right clearfix">
            <ul class="top-contact clearfix">
              <li class="t-email t-email1">
                <em class="fa fa-envelope-o" aria-hidden="true"></em>
                <span><a href="#">contact@youremail.com</a></span>
              </li>
              <li class="t-phone t-phone1">
                <em class="fa fa-phone" aria-hidden="true"></em>
                <span>+123-456-789</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- #end Topbar -->
    <!-- Navbar -->
    <div class="navbar navbar-primary">
      <div class="container">
        <!-- Logo -->
        <a class="navbar-brand" href="#">
          <img class="logo logo-dark" alt="" src="/images/logo2.png" srcset="/images/logo2.png 2x">
          <img class="logo logo-light" alt="" src="/images/logo2.pngg" srcset="/images/logo2.png 2x">
        </a>
        <!-- #end Logo -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav" aria-expanded="false">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- Q-Button for Mobile -->
          <div class="quote-btn"><a class="btn" href="get-a-quote.html">Sign in</a></div>
        </div>
        <!-- MainNav -->
        <nav class="navbar-collapse collapse" id="mainnav">
          <ul class="nav navbar-nav">
            <li class="<?= ($this->params['page'] == 'home') ? 'active' :'' ;?>"><a href="/">Home</a>
              <!-- <ul class="dropdown-menu">
                <li><a href="index.html">Home - Default</a></li>
                <li><a href="index-v2.html">Home - Version 2</a></li>
                <li><a href="index-v3.html">Home - Version 3</a></li>
                <li><a href="index-v4.html">Home - Version 4</a></li>
                <li><a href="index-static.html">Home - Image</a></li>
              </ul> -->
            </li>
            <li class="dropdown <?= ($this->params['page'] == 'about') ? 'active' :'' ;?>"><a href="/site/about">About us <!-- <b class="caret"></b> --></a>
              <!-- <ul class="dropdown-menu">
                <li><a href="faqs.html">FAQ's</a></li>
                <li><a href="teams.html">Teams</a></li>
                <li><a href="about-us-alter.html">About - Alter</a></li>
                <li><a href="teams-alter.html">Teams - Alter</a></li>
                <li><a href="contact-extend.html">Contact - Extend</a></li>
                <li><a href="shortcode.html">Shortcode <span class="label label-danger">Hot</span></a></li>
                <li><a href="typography.html">Typography</a></li>
              </ul> -->
            </li>
            <li class="dropdown <?= ($this->params['page'] == 'services') ? 'active' :'' ;?>">
              <a href="/site/services" class="dropdown-toggle">Services <!-- <b class="caret"></b> --></a>
              <!-- <ul class="dropdown-menu">
                <li><a href="service.html">Services List</a></li>
                <li><a href="service-alter.html">Services List - Alter</a></li>
                <li><a href="service-single.html">Services Single</a></li>
                <li><a href="service-single-alter.html">Services Single - Alter</a></li>
              </ul> -->
            </li>
            <li class="<?= ($this->params['page'] == 'voipsolutions') ? 'active' :'' ;?>"><a href="/site/voipsolutions">VOIP Solutions</a></li>
            <li class="<?= ($this->params['page'] == 'clients') ? 'active' :'' ;?>"><a href="/site/clients">Our Clients</a></li>
            <li class="<?= ($this->params['page'] == 'term') ? 'active' :'' ;?>"><a href="/site/term">Term & Condition</a></li>
            <li class="quote-btn <?= ($this->params['page'] == 'contact') ? 'active' :'' ;?>"><a class="btn" href="/site/contact">Contact</a></li>
          </ul>
        </nav>     
        <!-- #end MainNav -->
      </div>
    </div>

    <?= $content ?>
    <div class="section section-logos section-pad-sm bg-light bdr-top">
    <div class="container">
      <div class="content row">

        <div class="owl-carousel loop logo-carousel style-v2">
          <div class="logo-item"><img alt="" width="190" height="82" src="/images/380.jpg"></div>
          <div class="logo-item"><img alt="" width="190" height="82" src="/images/380-2.jpg"></div>
          <div class="logo-item"><img alt="" width="190" height="82" src="/images/380.jpg"></div>
          <div class="logo-item"><img alt="" width="190" height="82" src="/images/380-2.jpg"></div>
          <div class="logo-item"><img alt="" width="190" height="82" src="/images/380.jpg"></div>
          <div class="logo-item"><img alt="" width="190" height="82" src="/images/380-2.jpg"></div>
        </div>

      </div>
    </div>  
  </div>
  <!-- End Section -->

  <!-- Call Action -->
  <div class="call-action cta-small has-bg bg-primary" style="background-/images: url('/images/plx-cta.jpg');">
    <div class="cta-block">
      <div class="container">
        <div class="content row">

          <div class="cta-sameline">
            <h2>Have any Question?</h2>
            <p>We're here to help. Send us an email or call us at +012-345-6789. Please feel free to contact our expert.</p>
            <a class="btn btn-alt" href="#">Contact Us</a>
          </div>

        </div>
      </div>
    </div>
  </div>
    <div class="footer-widget style-v2 section-pad-md">
    <div class="container">
      <div class="row">

        <div class="widget-row row">
          <div class="footer-col col-md-3 col-sm-6 res-m-bttm">
            <!-- Each Widget -->
            <div class="wgs wgs-footer wgs-text">
              <div class="wgs-content">
                <p><img src="/images/logo2.png" srcset="/images/logo2x.png 2x" alt=""></p>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud. Voluptatem accusan tiudo lorem quveniamv eniam laud accusan tiud.</p>
              </div>
            </div>
            <!-- End Widget -->
          </div>
          <div class="footer-col col-md-3 col-sm-6 col-md-offset-1 res-m-bttm">
            <!-- Each Widget -->
            <div class="wgs wgs-footer wgs-menu">
              <h5 class="wgs-title">Our Services</h5>
              <div class="wgs-content">
                <ul class="menu">
                  <li><a href="#">Advisory</a></li>
                  <li><a href="#">Audit</a></li>
                  <li><a href="#">Consultent</a></li>
                  <li><a href="#">Assurance</a></li>
                  <li><a href="#">Funds</a></li>
                </ul>
              </div>
            </div>
            <!-- End Widget -->
          </div>
          <div class="footer-col col-md-2 col-sm-6 res-m-bttm">
            <!-- Each Widget -->
            <div class="wgs wgs-footer wgs-menu">
              <h5 class="wgs-title">Quick Links</h5>
              <div class="wgs-content">
                <ul class="menu">
                  <li><a href="/">Home</a></li>
                  <li><a href="/site/about">About Us</a></li>
                  <li><a href="/site/services">Services</a></li>
                  <li><a href="/site/voipsolutions">VOIP Solution</a></li>
                  <li><a href="/site/clients">Our Clients</a></li>
                  <li><a href="/site/term">Term & Condition</a></li>
                  <li><a href="/site/contact">Contact Us</a></li>
                </ul>
              </div>
            </div>
            <!-- End Widget -->
          </div>

          <div class="footer-col col-md-3 col-sm-6">
            <!-- Each Widget -->
            <div class="wgs wgs-footer">
              <h5 class="wgs-title">Get In Touch</h5>
              <div class="wgs-content">
                <p>
                  1234 Sed spiciatis Road <br>
                  Atero eos, D58 8975, USA.</p>
                <p><span>Toll Free</span>: (1-800) 234 5678<br>
                  <span>Phone</span>: (123) 1234 5678</p>
                <ul class="social">
                  <li><a href="#"><em class="fa fa-facebook" aria-hidden="true"></em></a></li>
                  <li><a href="#"><em class="fa fa-twitter" aria-hidden="true"></em></a></li>
                  <li><a href="#"><em class="fa fa-linkedin" aria-hidden="true"></em></a></li>
                </ul>
              </div>
            </div>
            <!-- End Widget -->
          </div>

        </div><!-- Widget Row -->

      </div>
    </div>
  </div>
  <!-- End Footer Widget -->

  <!-- Copyright -->
  <div class="copyright style-v2">
    <div class="container">
      <div class="row">
      
        <div class="row">
          <div class="site-copy col-sm-7">
            <p>&copy; 2017 Core3 Network Pvt. Ltd <a href="#">All Right Reserved.</a></p>
          </div>
          <div class="site-by col-sm-5 al-right">
            <p>Designed by :<a href="http://makeubig.com/" target="_blank">MakeUBIG</a></p>
          </div>
        </div>
                
      </div>
    </div>
  </div>
  <!-- End Copyright -->
  
  <!-- Rreload Image for Slider -->
  <div class="preload hide">
    <img alt="" src="/images/slider-lg-a.jpg">
    <img alt="" src="/images/slider-lg-b.jpg">
  </div>
  <!-- End -->

  <!-- Preloader !active please if you want -->
  <!-- <div id="preloader"><div id="status">&nbsp;</div></div> -->
  <!-- Preloader End -->

  <!-- JavaScript Bundle -->
 
  <!-- End script -->
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 ga('create', 'UA-104726724-1', 'auto');
 ga('send', 'pageview');
</script>
</body>
<?php $this->endBody();?>
</html>
<?php $this->endPage();?>
