<?php
namespace app\helpers;

class HtmlHelper {

public static function cutHtml($text, $max_length)
{
    $tags   = array();
    $result = "";

    $is_open   = false;
    $grab_open = false;
    $is_close  = false;
    $in_double_quotes = false;
    $in_single_quotes = false;
    $tag = "";

    $i = 0;
    $stripped = 0;

    $stripped_text = strip_tags($text);

    while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
    {
        $symbol  = $text{$i};
        $result .= $symbol;

        switch ($symbol)
        {
           case '<':
                $is_open   = true;
                $grab_open = true;
                break;

           case '"':
               if ($in_double_quotes)
                   $in_double_quotes = false;
               else
                   $in_double_quotes = true;

            break;

            case "'":
              if ($in_single_quotes)
                  $in_single_quotes = false;
              else
                  $in_single_quotes = true;

            break;

            case '/':
                if ($is_open && !$in_double_quotes && !$in_single_quotes)
                {
                    $is_close  = true;
                    $is_open   = false;
                    $grab_open = false;
                }

                break;

            case ' ':
                if ($is_open)
                    $grab_open = false;
                else
                    $stripped++;

                break;

            case '>':
                if ($is_open)
                {
                    $is_open   = false;
                    $grab_open = false;
                    array_push($tags, $tag);
                    $tag = "";
                }
                else if ($is_close)
                {
                    $is_close = false;
                    array_pop($tags);
                    $tag = "";
                }

                break;

            default:
                if ($grab_open || $is_close)
                    $tag .= $symbol;

                if (!$is_open && !$is_close)
                    $stripped++;
        }

        $i++;
    }

    while ($tags)
        $result .= "</".array_pop($tags).">";

    return $result;
	}

    public static function getColSize($colCount)
    {
        switch($colCount)
        {
             case 1 :
            {
                return 12;
            }
            break;
            case 2 :
            {
                return 6;
            }
            break;
            case 3 :
            {
                return 4;
            }
            break;
            case 4 :
            {
                return 3;
            }
            break;
            case 5 :
            {
                return 2;
            }
            break;
            case 6 :
            {
                return 2;
            }
            break;
            case 7 :
            {
                return 2;
            }
            break;
            case 8 :
            {
                return 2;
            }
            break;
            case 9 :
            {
                return 2;
            }
            break;
            case 10 :
            {
                return 2;
            }
            break;
            case 11 :
            {
                return 2;
            }
            break;
            case 12 :
            {
                return 2;
            }
            break;   
        }
    }	
}
